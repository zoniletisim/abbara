﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.IO;
using System.Web;
using System.Text.RegularExpressions;

namespace DAL
{
    public static class ExtensionMethod
    {
        public static string NullDate = "01.01.0001 00:00:00";
        public static string ConvertToUrl(string url)
        {
            string x = url.ToLower();
            x = x.Replace(" ", "-");
            x = x.Replace("/", "-");
            x = x.Replace("!", "");
            x = x.Replace("^", "");
            x = x.Replace("+", "");
            x = x.Replace("%", "");
            x = x.Replace("&", "");
            x = x.Replace("@", "");
            x = x.Replace("?", "-");
            x = x.Replace("!", "-");
            x = x.Replace(":", "-");
            x = x.Replace("<", "-");
            x = x.Replace(">", "-");
            x = x.Replace("(", "-");
            x = x.Replace(")", "-");
            x = x.Replace("{", "");
            x = x.Replace("}", "");
            x = x.Replace("[", "");
            x = x.Replace("]", "");
            x = x.Replace("‘", "");
            x = x.Replace("’", "");
            x = x.Replace("*", "");
            x = x.Replace(",", "-");
            x = x.Replace(".", "-");
            x = x.Replace('"', '-');
            x = x.Replace('“', '-');
            x = x.Replace('”', '-');
            x = x.Replace("~", "");
            x = x.Replace('=', '-');
            x = x.Replace("<", "");
            x = x.Replace(">", "");
            x = x.Replace("'", "");
            x = x.Replace("ş", "s");
            x = x.Replace("ğ", "g");
            x = x.Replace("ç", "c");
            x = x.Replace("ı", "i");
            x = x.Replace("ü", "u");
            x = x.Replace("ö", "o");
            x = x.Replace("--", "-");
            x = x.Replace("---", "-");
            x = x.Replace("----", "-");
            x = x.Replace("-----", "-");

            return x;
        }

        public static string ConvertToSocialMediaIcon(string name)
        {
            string x = name.ToLower();
            x = x.Replace(" ", "-");
            x = x.Replace("ı", "i");
            x = x.Replace("ü", "u");
            x = x.Replace("ö", "o");
            x = x.Replace("ş", "s");
            x = x.Replace("ğ", "g");
            x = x.Replace("ç", "c");

            return x;
        }

        public static string ConvertToEditor(string text)
        {
            string x = text;
            x = x.Replace("&#304;", "I");
            x = x.Replace("&#305;", "i");
            x = x.Replace("&#214;", "Ö");
            x = x.Replace("&#246;", "ö");
            x = x.Replace("&Ouml;", "Ö");
            x = x.Replace("&ouml;", "ö");
            x = x.Replace("&#220;", "Ü");
            x = x.Replace("&#252;", "ü");
            x = x.Replace("&Uuml;", "Ü");
            x = x.Replace("&uuml;", "ü");
            x = x.Replace("&#199;", "Ç");
            x = x.Replace("&#231;", "ç");
            x = x.Replace("&Ccedil;", "Ç");
            x = x.Replace("&ccedil;", "ç");
            x = x.Replace("&#286;", "G");
            x = x.Replace("&#287;", "g");
            x = x.Replace("&#350;", "S");
            x = x.Replace("&#351;", "s");
            return x;
        }

        public static string RemoveHtml(this string Html)
        {
            var result = String.Empty;
            if (!string.IsNullOrEmpty(Html))
            {
                result = Regex.Replace(Html, @"<(.|\n)*?>", "");
                result = Regex.Replace(result, "&Uuml;", "Ü");
                result = Regex.Replace(result, "&uuml;", "ü");
                result = Regex.Replace(result, "&Ouml;", "Ö");
                result = Regex.Replace(result, "&ouml;", "ö");
                result = Regex.Replace(result, "&Ccedil;", "Ç");
                result = Regex.Replace(result, "&ccedil;", "ç");
                result = Regex.Replace(result, "&nbsp;", " ");
                result = Regex.Replace(result, "&quot;", "'");
                result = Regex.Replace(result, "&rdquo;", "'");
                result = Regex.Replace(result, "&rsquo;", "'");
            }
            return result;
        }
        public static string DateToString(this DateTime? t)
        {
            if (t == null)
                return null;
            return Convert.ToDateTime(t).ToString("dd-MM-yyyy");
        }
        public static string DateToStringDay(this DateTime? t)
        {
            if (t == null)
                return null;
            return Convert.ToDateTime(t).ToString("dd");
        }
        public static int ToInt32(this string s)
        {
            if (s == null || s == "")
                return 0;
            else
                return Int32.Parse(s);
        }
        public static decimal ToDecimal(this string s)
        {
            if (s == null || s == "")
                return 0;
            return Convert.ToDecimal(s);

        }
        public static string ConvertString(this int i)
        {
            return i.ToString();
        }

        public static bool ConvertToBoolean(this string s)
        {
            if (s == "false")
                return false;
            else if (s == "true")
                return true;
            else
                return false;
        }

        public static DateTime ConvertDateTime(this string s)
        {
            if (!string.IsNullOrEmpty(s))
                return Convert.ToDateTime(s);
            else
                return Convert.ToDateTime(NullDate);
        }

        public static int ObjectConvertToInt(this object s)
        {
            if (s == null || s == "")
                return 0;
            return Convert.ToInt32(s);
        }
        public static string ToMonthName(this DateTime dateTime)
        {
            return CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dateTime.Month);
        }

        public static string ToShortMonthName(this DateTime dateTime)
        {
            return CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(dateTime.Month);
        }
        public static string IllegalCharRemove(this object s)
        {
            var x = s;
            if (s == null)
                return "";
            x = x.ToString().Replace('-', ' ');
            x = x.ToString().Replace('!', ' ');
            x = x.ToString().Replace('^', ' ');
            x = x.ToString().Replace('+', ' ');
            x = x.ToString().Replace('%', ' ');
            x = x.ToString().Replace('&', ' ');
            x = x.ToString().Replace('/', ' ');
            x = x.ToString().Replace('(', ' ');
            x = x.ToString().Replace(')', ' ');
            x = x.ToString().Replace('=', ' ');
            x = x.ToString().Replace('*', ' ');
            x = x.ToString().Replace(',', ' ');
            x = x.ToString().Replace('.', ' ');
            x = x.ToString().Replace('ç', ' ');
            x = x.ToString().Replace('"', ' ');
            x = x.ToString().Replace('~', ' ');
            x = x.ToString().Replace("'", "");
            x = x.ToString().Replace("q", "");
            x = x.ToString().Replace("w", "");
            x = x.ToString().Replace("e", "");
            x = x.ToString().Replace("r", "");
            x = x.ToString().Replace("t", "");
            x = x.ToString().Replace("y", "");
            x = x.ToString().Replace("u", "");
            x = x.ToString().Replace("ı", "");
            x = x.ToString().Replace("o", "");
            x = x.ToString().Replace("p", "");
            x = x.ToString().Replace("ğ", "");
            x = x.ToString().Replace("ü", "");
            x = x.ToString().Replace("a", "");
            x = x.ToString().Replace("s", "");
            x = x.ToString().Replace("d", "");
            x = x.ToString().Replace("f", "");
            x = x.ToString().Replace("g", "");
            x = x.ToString().Replace("h", "");
            x = x.ToString().Replace("j", "");
            x = x.ToString().Replace("k", "");
            x = x.ToString().Replace("l", "");
            x = x.ToString().Replace("ş", "");
            x = x.ToString().Replace("i", "");
            x = x.ToString().Replace(",", "");
            x = x.ToString().Replace("<", "");
            x = x.ToString().Replace(">", "");
            x = x.ToString().Replace("z", "");
            x = x.ToString().Replace("x", "");
            x = x.ToString().Replace("c", "");
            x = x.ToString().Replace("v", "");
            x = x.ToString().Replace("b", "");
            x = x.ToString().Replace("n", "");
            x = x.ToString().Replace("m", "");
            x = x.ToString().Replace("ö", "");
            x = x.ToString().Replace("ç", "");
            x = x.ToString().Replace(".", "");


            return x.ToString();
        }
        public static bool EnumToBoolean(this int x)
        {
            if (x == 0)
                return false;
            return true;
        }
        public static string PathAdd(this string s)
        {
            var AddPath = StaticData.ImageSite;
            var Content = "/Content/";
            s = s.Replace("/Content/", AddPath + Content);
            return s;
        }
        public static void ErrorWrite(Exception ex)
        {
            if (!File.Exists(HttpContext.Current.Server.MapPath("~/ErrorLog/log.txt")))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/ErrorLog/"));
                FileStream file = File.Create(HttpContext.Current.Server.MapPath("~/ErrorLog/log.txt"));
                file.Flush();
                file.Dispose();
                file.Close();
            }

            using (StreamWriter sw = File.AppendText(HttpContext.Current.Server.MapPath("~/ErrorLog/log.txt")))
            {
                sw.WriteLine("Ex Message: " + ex.Message);
                sw.WriteLine("Inner Message: " + ex.InnerException);
                sw.WriteLine("Stack Trace: " + ex.StackTrace);
                sw.WriteLine("Date-Time: " + DateTime.Now);
                sw.WriteLine("--------------------------------------------------------------------------------------------------------------------");
                sw.WriteLine("--------------------------------------------------------------------------------------------------------------------");
                sw.Flush();
                sw.Close();
            }
        }
    }

    public class MyClass
    {
        public DataTable ListConvertToDataTable<T>(List<T> obj) where T : class
        {
            DataTable dt = new DataTable();

            var properties = obj.FirstOrDefault();

            Type t = properties.GetType();

            var columns = t.GetProperties().Select(a => a.Name).ToList();

            foreach (var item in columns)
            {
                dt.Columns.Add(item);
            }

            foreach (var item in obj)
            {
                var type = item.GetType();
                for (int k = 0; k < columns.Count; k++)
                {
                    dt.Rows.Add(type.GetProperty(columns[k]).GetValue(item));
                }
            }

            return dt;

            //var attachment = "attachment; filename=city.xls";
            //HttpContext.Current.Response.ClearContent();
            //HttpContext.Current.Response.AddHeader("content-disposition",attachment);
            //HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            //var tab = "";
            //foreach(DataColumn dc in dt.Columns)
            //{
            //    HttpContext.Current.Response.Write(tab + dc.ColumnName);
            //    tab = "\t";
            //}
            //HttpContext.Current.Response.Write("\n");
            //int i;
            //foreach(DataRow dr in dt.Rows)
            //{
            //    tab = "";
            //    for(i = 0;i < dt.Columns.Count;i++)
            //    {
            //        HttpContext.Current.Response.Write(tab + dr[ i ].ToString());
            //        tab = "\t";
            //    }
            //    HttpContext.Current.Response.Write("\n");
            //}
            //HttpContext.Current.Response.End();
        }
    }

}
public class StringOperations
{
    /// <summary>
    /// Produces optional, URL-friendly version of a title, "like-this-one". 
    /// hand-tuned for speed, reflects performance refactoring contributed
    /// by John Gietzen (user otac0n) 
    /// </summary>
    public static string URLFriendly(string title)
    {
        if (title == null) return "";

        const int maxlen = 80;
        int len = title.Length;
        bool prevdash = false;
        var sb = new StringBuilder(len);
        char c;

        for (int i = 0; i < len; i++)
        {
            c = title[i];
            if ((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9'))
            {
                sb.Append(c);
                prevdash = false;
            }
            else if (c >= 'A' && c <= 'Z')
            {
                // tricky way to convert to lowercase
                sb.Append((char)(c | 32));
                prevdash = false;
            }
            else if (c == ' ' || c == ',' || c == '.' || c == '/' ||
                c == '\\' || c == '-' || c == '_' || c == '=')
            {
                if (!prevdash && sb.Length > 0)
                {
                    sb.Append('-');
                    prevdash = true;
                }
            }
            else if ((int)c >= 128)
            {
                int prevlen = sb.Length;
                sb.Append(RemapInternationalCharToAscii(c));
                if (prevlen != sb.Length) prevdash = false;
            }
            if (i == maxlen) break;
        }

        if (prevdash)
            return sb.ToString().Substring(0, sb.Length - 1);
        else
            return sb.ToString();
    }
    public static string GetPartTextInContent(string s, int len)
    {
        if (s != null && s != "")
        {
            var result = String.Empty;


            if (s.Length > len)
            {
                result = s.Substring(0, len);
                result += "...";
                return result;
            }
            else
                result = s;
            result += "...";
            return result;
        }
        else
            return "Kısa İçerik Yok...";

    }
    private static string RemapInternationalCharToAscii(char c)
    {
        string s = c.ToString().ToLowerInvariant();
        if ("àåáâäãåą".Contains(s))
        {
            return "a";
        }
        else if ("èéêëę".Contains(s))
        {
            return "e";
        }
        else if ("ìíîïı".Contains(s))
        {
            return "i";
        }
        else if ("òóôõöøőð".Contains(s))
        {
            return "o";
        }
        else if ("ùúûüŭů".Contains(s))
        {
            return "u";
        }
        else if ("çćčĉ".Contains(s))
        {
            return "c";
        }
        else if ("żźž".Contains(s))
        {
            return "z";
        }
        else if ("śşšŝ".Contains(s))
        {
            return "s";
        }
        else if ("ñń".Contains(s))
        {
            return "n";
        }
        else if ("ýÿ".Contains(s))
        {
            return "y";
        }
        else if ("ğĝ".Contains(s))
        {
            return "g";
        }
        else if (c == 'ř')
        {
            return "r";
        }
        else if (c == 'ł')
        {
            return "l";
        }
        else if (c == 'đ')
        {
            return "d";
        }
        else if (c == 'ß')
        {
            return "ss";
        }
        else if (c == 'Þ')
        {
            return "th";
        }
        else if (c == 'ĥ')
        {
            return "h";
        }
        else if (c == 'ĵ')
        {
            return "j";
        }
        else
        {
            return "";
        }
    }
}