//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.Repo
{
    using System;
    using System.Collections.Generic;
    
    public partial class WorkshopForm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Birthday { get; set; }
        public string Gender { get; set; }
        public string IdentificationNumber { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string District { get; set; }
        public string PostalCode { get; set; }
        public string Address { get; set; }
        public string Sector { get; set; }
        public string EducationDegree { get; set; }
        public string GraduatedSchool { get; set; }
        public string GraduatedSchoolSection { get; set; }
        public string CurrentSchool { get; set; }
        public string CurrentSchoolSection { get; set; }
        public string Program { get; set; }
        public string Informed { get; set; }
        public string WebSite { get; set; }
        public string Cv { get; set; }
        public string Allergy { get; set; }
        public string Nutrition { get; set; }
        public string RelationNameSurname { get; set; }
        public string RelationProximity { get; set; }
        public string RelationPhone { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<bool> Read { get; set; }
        public Nullable<bool> Deleted { get; set; }
    }
}
