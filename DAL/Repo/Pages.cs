//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.Repo
{
    using System;
    using System.Collections.Generic;
    
    public partial class Pages
    {
        public int Id { get; set; }
        public Nullable<int> DataId { get; set; }
        public Nullable<int> LanguageId { get; set; }
        public Nullable<int> ParentId { get; set; }
        public string Title { get; set; }
        public string ShortContent { get; set; }
        public string Content { get; set; }
        public string Banner { get; set; }
        public string Thumbnail { get; set; }
        public string Image { get; set; }
        public string Galleries { get; set; }
        public string Video { get; set; }
        public string File { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.TimeSpan> StartTime { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Link { get; set; }
        public Nullable<int> FormId { get; set; }
        public Nullable<bool> ShowMenu { get; set; }
        public string FriendlyUrl { get; set; }
        public string Keywords { get; set; }
        public string Description { get; set; }
        public Nullable<int> Order { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<int> Owner { get; set; }
        public Nullable<bool> Active { get; set; }
        public Nullable<bool> Deleted { get; set; }
    }
}
