﻿using DAL.Repo;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
namespace DAL
{
    public class AdminMainProgress
    {
        private int _login;

        public List<PanelMenu> PanelMenu { get; set; }
        public List<PanelUsers> PanelUsers { get; set; }
        public List<SocialMedia> SocialMedia { get; set; }
        public List<Languages> Languages { get; set; }
        public List<MultiPreferences> MultiPreferences { get; set; }
        public List<PeopleCategories> PeopleCategories { get; set; }
        public List<Peoples> Peoples { get; set; }
        public List<GalleryParentCategories> GalleryParentCategories { get; set; }
        public List<GalleryCategories> GalleryCategories { get; set; }
        public List<Gallery> Gallery { get; set; }
        public List<Pages> Pages { get; set; }
        public List<Events> Events { get; set; }
        public List<SupporterCategories> SupporterCategories { get; set; }
        public List<Supporters> Supporters { get; set; }
        public List<EventCategories> EventCategories { get; set; }
        public List<Press> Press { get; set; }
        public List<Translates> Translates { get; set; }
        public List<WorkshopForm> WorkshopForm { get; set; }
        public List<Newsletter> Newsletter { get; set; }
        public List<ContactForm> ContactForm { get; set; }
        public List<VolunteerForm> VolunteerForm { get; set; }

        DatabaseConnection db = new DatabaseConnection();

        public AdminMainProgress(object login)
        {
            this._login = login.ObjectConvertToInt();
        }

        public static PanelUsers AdminLogin(string email, string password, DatabaseConnection db)
        {
            string hashedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "SHA1");
            var user = db.PanelUsers.Where(x => x.Email == email && x.Password == hashedPassword && x.Active == true && x.Deleted == false).FirstOrDefault();
            return user;
        }

        //PanelUser Start
        public List<PanelUsers> GetPanelUsers()
        {
            var list = db.PanelUsers.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void PanelUsersSave(string Email, string Password, string Name, string Surname, int Role, int Owner, string Active, object login, DatabaseConnection db)
        {
            var isHave = db.PanelUsers.Where(x => x.Email == Email && x.Deleted == false).FirstOrDefault();

            if (isHave == null)
            {
                string hashedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(Password, "SHA1");
                var DataActive = false;
                if (Active == "0")
                    DataActive = true;

                var paneluser = new PanelUsers();
                paneluser.Email = Email;
                paneluser.Password = hashedPassword;
                paneluser.Name = Name;
                paneluser.Surname = Surname;
                paneluser.Role = Role;
                paneluser.Owner = Owner;
                paneluser.CreatedDate = DateTime.Now;
                paneluser.Active = DataActive;
                paneluser.Deleted = false;
                db.PanelUsers.Add(paneluser);
                db.SaveChanges();
            }
        }

        public static void PanelUsersUpdate(string Id, string Email, string Name, string Surname, int Role, int Owner, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var isHave = db.PanelUsers.Where(x => x.Email == Email && x.Deleted == false && x.Id != ConvertId).FirstOrDefault();

            if (isHave == null)
            {
                var DataActive = false;
                if (Active == "0")
                    DataActive = true;

                var paneluser = db.PanelUsers.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
                if (paneluser != null)
                {
                    paneluser.Email = Email;
                    paneluser.Name = Name;
                    paneluser.Surname = Surname;
                    paneluser.Role = Role;
                    paneluser.Owner = Owner;
                    paneluser.UpdatedDate = DateTime.Now;
                    paneluser.Active = DataActive;
                    db.SaveChanges();
                }
            }
        }

        public static string PanelUsersDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.PanelUsers.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool PanelUsersIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var paneluserUpdate = db.PanelUsers.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            paneluserUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }
        //PanelUser End

        //PanelMenu Start
        public List<PanelMenu> GetPanelMenu()
        {
            var list = db.PanelMenu.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void PanelMenuSave(int ParentId, string Name, string Url, string Redirect, int Access, string Active, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var panelmenu = new PanelMenu();
            panelmenu.ParentId = ParentId;
            panelmenu.Name = Name;
            panelmenu.Url = Url;
            panelmenu.Redirect = Redirect;
            panelmenu.Icon = "dash";
            panelmenu.Access = Access;
            panelmenu.Order = db.PanelMenu.Count() + 1;
            panelmenu.Active = DataActive;
            panelmenu.Deleted = false;
            db.PanelMenu.Add(panelmenu);
            db.SaveChanges();
        }

        public static void PanelMenuUpdate(string Id, int ParentId, string Name, string Url, string Redirect, int Access, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var panelmenu = db.PanelMenu.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            if (panelmenu != null)
            {
                panelmenu.ParentId = ParentId;
                panelmenu.Name = Name;
                panelmenu.Url = Url;
                panelmenu.Redirect = Redirect;
                panelmenu.Icon = "dash";
                panelmenu.Access = Access;
                panelmenu.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string PanelMenuDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.PanelMenu.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool PanelMenuIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var panelmenuUpdate = db.PanelMenu.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            panelmenuUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool PanelMenuSort(List<int> ids, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.PanelMenu.Where(x => x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //PanelMenu End

        //SocialMedia Start
        public List<SocialMedia> GetSocialMedia()
        {
            var list = db.SocialMedia.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void SocialMediaSave(string Name, string Link, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var socialmedia = new SocialMedia();
            socialmedia.Name = Name;
            socialmedia.Link = Link;
            socialmedia.Order = db.SocialMedia.Count() + 1;
            socialmedia.CreatedDate = DateTime.Now;
            socialmedia.Owner = Owner;
            socialmedia.Active = DataActive;
            socialmedia.Deleted = false;
            db.SocialMedia.Add(socialmedia);
            db.SaveChanges();
        }

        public static void SocialMediaUpdate(string Id, string Name, string Link, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var socialmedia = db.SocialMedia.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            if (socialmedia != null)
            {
                socialmedia.Name = Name;
                socialmedia.Link = Link;
                socialmedia.UpdatedDate = DateTime.Now;
                socialmedia.Owner = Owner;
                socialmedia.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string SocialMediaDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.SocialMedia.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool SocialMediaIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var socialmediaUpdate = db.SocialMedia.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            socialmediaUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool SocialMediaSort(List<int> ids, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.SocialMedia.Where(x => x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //SocialMedia End

        //Languages Start
        public List<Languages> GetLanguages()
        {
            var list = db.Languages.Where(x => x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public static void LanguagesSave(string Name, string ShortName, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var languages = new Languages();
            languages.Name = Name;
            languages.ShortName = ShortName;
            languages.Order = db.Languages.Count() + 1;
            languages.CreatedDate = DateTime.Now;
            languages.Owner = Owner;
            languages.Active = DataActive;
            languages.Deleted = false;
            db.Languages.Add(languages);
            db.SaveChanges();
        }

        public static void LanguagesUpdate(string Id, string Name, string ShortName, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var languages = db.Languages.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            if (languages != null)
            {
                languages.Name = Name;
                languages.ShortName = ShortName;
                languages.UpdatedDate = DateTime.Now;
                languages.Owner = Owner;
                languages.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string LanguagesDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Languages.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool LanguagesIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var languagesUpdate = db.Languages.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            languagesUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool LanguagesSort(List<int> ids, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.Languages.Where(x => x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //Languages End

        public object SelectLanguage(object LanguageId)
        {
            if (LanguageId == null)
            {
                LanguageId = 1;
            }

            return LanguageId;
        }

        //MultiPreferences Start
        public List<MultiPreferences> GetMultiPreferences()
        {
            var list = db.MultiPreferences.ToList();
            return list;
        }

        public static void MultiPreferencesUpdate(string Id, string Title, string Description, string Keywords, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();

            var multipreferences = db.MultiPreferences.Where(x => x.LanguageId == ConvertId).FirstOrDefault();
            if (multipreferences != null)
            {
                multipreferences.Title = Title;
                multipreferences.Description = Description;
                multipreferences.Keywords = Keywords;
                multipreferences.UpdatedDate = DateTime.Now;
                multipreferences.Owner = Owner;
                db.SaveChanges();
            }
        }
        //MultiPreferences End

        //PeopleCategories Start
        public List<PeopleCategories> GetPeopleCategories()
        {
            var list = db.PeopleCategories.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void PeopleCategoriesSave(int DataId, int LanguageId, int ParentId, string PageFriendlyUrl, string Title, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var peoplecategories = new PeopleCategories();
            if (DataId != 0) peoplecategories.DataId = DataId;
            peoplecategories.LanguageId = LanguageId;
            peoplecategories.ParentId = ParentId;
            peoplecategories.PageFriendlyUrl = PageFriendlyUrl;
            peoplecategories.Title = Title;
            peoplecategories.Order = db.PeopleCategories.Count() + 1;
            peoplecategories.CreatedDate = DateTime.Now;
            peoplecategories.Owner = Owner;
            peoplecategories.Active = DataActive;
            peoplecategories.Deleted = false;
            db.PeopleCategories.Add(peoplecategories);
            db.SaveChanges();

            if (DataId == 0)
            {
                var peoplecategories2 = db.PeopleCategories.Where(x => x.Id == peoplecategories.Id).FirstOrDefault();
                peoplecategories2.DataId = peoplecategories.Id;
                db.SaveChanges();
            }
        }

        public static void PeopleCategoriesUpdate(string Id, int DataId, int LanguageId, int ParentId, string PageFriendlyUrl, string Title, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var peoplecategories = db.PeopleCategories.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (peoplecategories != null)
            {
                peoplecategories.DataId = DataId;
                peoplecategories.ParentId = ParentId;
                peoplecategories.PageFriendlyUrl = PageFriendlyUrl;
                peoplecategories.Title = Title;
                peoplecategories.UpdatedDate = DateTime.Now;
                peoplecategories.Owner = Owner;
                peoplecategories.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string PeopleCategoriesDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.PeopleCategories.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool PeopleCategoriesIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var peoplecategoriesUpdate = db.PeopleCategories.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            peoplecategoriesUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool PeopleCategoriesSort(List<int> ids, int LanguageId, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.PeopleCategories.Where(x => x.LanguageId == LanguageId && x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //PeopleCategories End

        //Peoples Start
        public List<Peoples> GetPeoples()
        {
            var list = db.Peoples.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void PeoplesSave(int DataId, int LanguageId, int ParentId, string Name, string Surname, string Title, string Email, string Image, string ShortContent, string Content, string FriendlyUrl, string Keywords, string Description, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var peoples = new Peoples();
            if (DataId != 0) peoples.DataId = DataId;
            peoples.LanguageId = LanguageId;
            peoples.ParentId = ParentId;
            peoples.Name = Name;
            peoples.Surname = Surname;
            peoples.Title = Title;
            peoples.Email = Email;
            peoples.Image = Image;
            peoples.ShortContent = ShortContent;
            peoples.Content = Content;
            peoples.Keywords = Keywords;
            peoples.Description = Description;
            peoples.Order = db.Peoples.Count() + 1;
            peoples.CreatedDate = DateTime.Now;
            peoples.Owner = Owner;
            peoples.Active = DataActive;
            peoples.Deleted = false;

            //Friendly Url
            if (FriendlyUrl == string.Empty)
            {
                FriendlyUrl = Name + "-" + Surname;
            }

            var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            int HaveUrl = db.Peoples.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

            if (HaveUrl == 0)
            {
                peoples.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            }
            else
            {
                for (int i = 0; i < 999; i++)
                {
                    HaveUrl = db.Peoples.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                    if (HaveUrl == 0)
                    {
                        peoples.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                        break;
                    }
                }
            }
            //Friendly Url

            db.Peoples.Add(peoples);
            db.SaveChanges();

            if (DataId == 0)
            {
                var peoples2 = db.Peoples.Where(x => x.Id == peoples.Id).FirstOrDefault();
                peoples2.DataId = peoples.Id;
                db.SaveChanges();
            }
        }

        public static void PeoplesUpdate(string Id, int DataId, int LanguageId, int ParentId, string Name, string Surname, string Title, string Email, string Image, string ShortContent, string Content, string FriendlyUrl, string Keywords, string Description, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var peoples = db.Peoples.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (peoples != null)
            {
                peoples.DataId = DataId;
                peoples.ParentId = ParentId;
                peoples.Name = Name;
                peoples.Surname = Surname;
                peoples.Title = Title;
                peoples.Email = Email;
                peoples.Image = Image;
                peoples.ShortContent = ShortContent;
                peoples.Content = Content;
                peoples.Keywords = Keywords;
                peoples.Description = Description;
                peoples.UpdatedDate = DateTime.Now;
                peoples.Owner = Owner;
                peoples.Active = DataActive;

                //Friendly Url
                if (FriendlyUrl == string.Empty)
                {
                    FriendlyUrl = Name + "-" + Surname;
                }

                var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                int HaveUrl = db.Peoples.Where(x => x.DataId != ConvertId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

                if (HaveUrl == 0)
                {
                    peoples.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                }
                else
                {
                    for (int i = 0; i < 999; i++)
                    {
                        HaveUrl = db.Peoples.Where(x => x.DataId != ConvertId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                        if (HaveUrl == 0)
                        {
                            peoples.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                            break;
                        }
                    }
                }
                //Friendly Url

                db.SaveChanges();
            }
        }

        public static string PeoplesDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Peoples.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool PeoplesIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var peoplesUpdate = db.Peoples.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            peoplesUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool PeoplesSort(List<int> ids, int LanguageId, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.Peoples.Where(x => x.LanguageId == LanguageId && x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //Peoples End

        //GalleryParentCategories Start
        public List<GalleryParentCategories> GetGalleryParentCategories()
        {
            var list = db.GalleryParentCategories.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void GalleryParentCategoriesSave(int DataId, int LanguageId, string Title, string FriendlyUrl, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var galleryparentcategories = new GalleryParentCategories();
            if (DataId != 0) galleryparentcategories.DataId = DataId;
            galleryparentcategories.LanguageId = LanguageId;
            galleryparentcategories.Title = Title;
            galleryparentcategories.Order = db.GalleryParentCategories.Count() + 1;
            galleryparentcategories.CreatedDate = DateTime.Now;
            galleryparentcategories.Owner = Owner;
            galleryparentcategories.Active = DataActive;
            galleryparentcategories.Deleted = false;

            //Friendly Url
            if (FriendlyUrl == string.Empty)
            {
                FriendlyUrl = Title;
            }

            var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            int HaveUrl = db.GalleryParentCategories.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

            if (HaveUrl == 0)
            {
                galleryparentcategories.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            }
            else
            {
                for (int i = 0; i < 999; i++)
                {
                    HaveUrl = db.GalleryParentCategories.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                    if (HaveUrl == 0)
                    {
                        galleryparentcategories.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                        break;
                    }
                }
            }
            //Friendly Url

            db.GalleryParentCategories.Add(galleryparentcategories);
            db.SaveChanges();

            if (DataId == 0)
            {
                var galleryparentcategories2 = db.GalleryParentCategories.Where(x => x.Id == galleryparentcategories.Id).FirstOrDefault();
                galleryparentcategories2.DataId = galleryparentcategories.Id;
                db.SaveChanges();
            }
        }

        public static void GalleryParentCategoriesUpdate(string Id, int DataId, int LanguageId, string Title, string FriendlyUrl, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var galleryparentcategories = db.GalleryParentCategories.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (galleryparentcategories != null)
            {
                galleryparentcategories.DataId = DataId;
                galleryparentcategories.Title = Title;
                galleryparentcategories.UpdatedDate = DateTime.Now;
                galleryparentcategories.Owner = Owner;
                galleryparentcategories.Active = DataActive;

                //Friendly Url
                if (FriendlyUrl == string.Empty)
                {
                    FriendlyUrl = Title;
                }

                var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                int HaveUrl = db.GalleryParentCategories.Where(x => x.DataId != ConvertId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

                if (HaveUrl == 0)
                {
                    galleryparentcategories.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                }
                else
                {
                    for (int i = 0; i < 999; i++)
                    {
                        HaveUrl = db.GalleryParentCategories.Where(x => x.DataId != ConvertId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                        if (HaveUrl == 0)
                        {
                            galleryparentcategories.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                            break;
                        }
                    }
                }
                //Friendly Url

                db.SaveChanges();
            }
        }

        public static string GalleryParentCategoriesDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.GalleryParentCategories.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool GalleryParentCategoriesIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var galleryparentcategoriesUpdate = db.GalleryParentCategories.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            galleryparentcategoriesUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool GalleryParentCategoriesSort(List<int> ids, int LanguageId, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.GalleryParentCategories.Where(x => x.LanguageId == LanguageId && x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //GalleryParentCategories End

        //GalleryCategories Start
        public List<GalleryCategories> GetGalleryCategories()
        {
            var list = db.GalleryCategories.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void GalleryCategoriesSave(int DataId, int LanguageId, int ParentId, string Title, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var GalleryCategories = new GalleryCategories();
            if (DataId != 0) GalleryCategories.DataId = DataId;
            GalleryCategories.LanguageId = LanguageId;
            GalleryCategories.ParentId = ParentId;
            GalleryCategories.Title = Title;
            GalleryCategories.Order = db.GalleryCategories.Count() + 1;
            GalleryCategories.CreatedDate = DateTime.Now;
            GalleryCategories.Owner = Owner;
            GalleryCategories.Active = DataActive;
            GalleryCategories.Deleted = false;
            db.GalleryCategories.Add(GalleryCategories);
            db.SaveChanges();

            if (DataId == 0)
            {
                var GalleryCategories2 = db.GalleryCategories.Where(x => x.Id == GalleryCategories.Id).FirstOrDefault();
                GalleryCategories2.DataId = GalleryCategories.Id;
                db.SaveChanges();
            }
        }

        public static void GalleryCategoriesUpdate(string Id, int DataId, int LanguageId, int ParentId, string Title, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var GalleryCategories = db.GalleryCategories.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (GalleryCategories != null)
            {
                GalleryCategories.DataId = DataId;
                GalleryCategories.ParentId = ParentId;
                GalleryCategories.Title = Title;
                GalleryCategories.UpdatedDate = DateTime.Now;
                GalleryCategories.Owner = Owner;
                GalleryCategories.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string GalleryCategoriesDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.GalleryCategories.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool GalleryCategoriesIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var GalleryCategoriesUpdate = db.GalleryCategories.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            GalleryCategoriesUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool GalleryCategoriesSort(List<int> ids, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.GalleryCategories.Where(x => x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //GalleryCategories End

        //Gallery Start
        public List<Gallery> GetGallery()
        {
            var list = db.Gallery.ToList();
            return list;
        }

        public static void GallerySave(int ParentId, string Image, int Owner, object login, DatabaseConnection db)
        {
            var gallery = new Gallery();
            gallery.ParentId = ParentId;
            gallery.Image = Image;
            gallery.Order = db.Gallery.Count() + 1;
            gallery.CreatedDate = DateTime.Now;
            gallery.Owner = Owner;
            db.Gallery.Add(gallery);
            db.SaveChanges();
        }

        public static string GalleryDelete(object login, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = id;
            var action = QueryString;
            var Id = id.IllegalCharRemove().ObjectConvertToInt();
            if (Id > 0)
            {
                var which = login.ObjectConvertToInt();
                var faq = db.Gallery.Where(x => x.Id == Id).FirstOrDefault();
                db.Gallery.Remove(faq);
                db.SaveChanges();
                return url;
            }
            return url = null;
        }

        public static bool GallerySort(List<int> ids, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.Gallery.ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //Gallery End

        //Pages Start
        public List<Pages> GetPages()
        {
            var list = db.Pages.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void PagesSave(int DataId, int LanguageId, int ParentId, string Title, string ShortContent, string Content, string Banner, string Thumbnail, string Image, string Galleries, string Video, string File, string StartDate, string StartTime, string EndDate, string Link, int FormId, string ShowMenu, string FriendlyUrl, string Keywords, string Description, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var DataShowMenu = false;
            if (ShowMenu == "0")
                DataShowMenu = true;

            var pages = new Pages();
            if (DataId != 0) pages.DataId = DataId;
            pages.LanguageId = LanguageId;
            pages.ParentId = ParentId;
            pages.Title = Title;
            pages.ShortContent = ShortContent;
            pages.Content = Content;
            pages.Banner = Banner;
            pages.Thumbnail = Thumbnail;
            pages.Image = Image;
            pages.Galleries = Galleries;
            pages.Video = Video;
            pages.File = File;
            if (StartDate.ToString() != string.Empty) pages.StartDate = Convert.ToDateTime(StartDate);
            if (StartTime.ToString() != string.Empty) pages.StartTime = TimeSpan.Parse(StartTime);
            if (EndDate.ToString() != string.Empty) pages.EndDate = Convert.ToDateTime(EndDate);
            pages.Link = Link;
            pages.FormId = FormId;
            pages.ShowMenu = DataShowMenu;
            pages.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            pages.Keywords = Keywords;
            pages.Description = Description;
            pages.Order = db.Pages.Count() + 1;
            pages.CreatedDate = DateTime.Now;
            pages.Owner = Owner;
            pages.Active = DataActive;
            pages.Deleted = false;

            //Friendly Url
            var ParentData = db.Pages.Where(x => x.DataId == ParentId && x.LanguageId == LanguageId).FirstOrDefault();

            if (FriendlyUrl == string.Empty)
            {
                if (ParentData != null)
                {
                    FriendlyUrl = ParentData.Title + " " + Title;
                }
                else
                {
                    FriendlyUrl = Title;
                }
            }

            var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            int HaveUrl = db.Pages.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

            if (HaveUrl == 0)
            {
                pages.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            }
            else
            {
                for (int i = 0; i < 999; i++)
                {
                    HaveUrl = db.Pages.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                    if (HaveUrl == 0)
                    {
                        pages.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                        break;
                    }
                }
            }
            //Friendly Url

            db.Pages.Add(pages);
            db.SaveChanges();

            if (DataId == 0)
            {
                var pages2 = db.Pages.Where(x => x.Id == pages.Id).FirstOrDefault();
                pages2.DataId = pages.Id;
                db.SaveChanges();
            }
        }

        public static void PagesUpdate(string Id, int DataId, int LanguageId, int ParentId, string Title, string ShortContent, string Content, string Banner, string Thumbnail, string Image, string Galleries, string Video, string File, string StartDate, string StartTime, string EndDate, string Link, int FormId, string ShowMenu, string FriendlyUrl, string Keywords, string Description, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;
            var DataShowMenu = false;
            if (ShowMenu == "0")
                DataShowMenu = true;

            var pages = db.Pages.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (pages != null)
            {
                pages.DataId = DataId;
                pages.ParentId = ParentId;
                pages.Title = Title;
                pages.ShortContent = ShortContent;
                pages.Content = Content;
                pages.Banner = Banner;
                pages.Thumbnail = Thumbnail;
                pages.Image = Image;
                pages.Galleries = Galleries;
                pages.Video = Video;
                pages.File = File;
                if (StartDate.ToString() != string.Empty) pages.StartDate = Convert.ToDateTime(StartDate);
                if (StartTime.ToString() != string.Empty) pages.StartTime = TimeSpan.Parse(StartTime);
                if (EndDate.ToString() != string.Empty) pages.EndDate = Convert.ToDateTime(EndDate);
                pages.Link = Link;
                pages.FormId = FormId;
                pages.ShowMenu = DataShowMenu;
                pages.Keywords = Keywords;
                pages.Description = Description;
                pages.UpdatedDate = DateTime.Now;
                pages.Owner = Owner;
                pages.Active = DataActive;

                //Friendly Url
                var ParentData = db.Pages.Where(x => x.DataId == ParentId && x.LanguageId == LanguageId).FirstOrDefault();

                if (FriendlyUrl == string.Empty)
                {
                    if (ParentData != null)
                    {
                        FriendlyUrl = ParentData.Title + " " + Title;
                    }
                    else
                    {
                        FriendlyUrl = Title;
                    }
                }

                var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                int HaveUrl = db.Pages.Where(x => x.DataId != ConvertId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

                if (HaveUrl == 0)
                {
                    pages.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                }
                else
                {
                    for (int i = 0; i < 999; i++)
                    {
                        HaveUrl = db.Pages.Where(x => x.DataId != ConvertId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                        if (HaveUrl == 0)
                        {
                            pages.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                            break;
                        }
                    }
                }
                //Friendly Url

                db.SaveChanges();
            }
        }

        public static string PagesDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Pages.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool PagesIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var pagesUpdate = db.Pages.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            pagesUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool PagesSort(List<int> ids, int LanguageId, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.Pages.Where(x => x.LanguageId == LanguageId && x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //Pages End

        //SupporterCategories Start
        public List<SupporterCategories> GetSupporterCategories()
        {
            var list = db.SupporterCategories.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void SupporterCategoriesSave(int DataId, int LanguageId, int TypeId, string Title, string Content, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var supportercategories = new SupporterCategories();
            if (DataId != 0) supportercategories.DataId = DataId;
            supportercategories.LanguageId = LanguageId;
            supportercategories.TypeId = TypeId;
            supportercategories.Title = Title;
            supportercategories.Content = Content;
            supportercategories.Order = db.SupporterCategories.Count() + 1;
            supportercategories.CreatedDate = DateTime.Now;
            supportercategories.Owner = Owner;
            supportercategories.Active = DataActive;
            supportercategories.Deleted = false;
            db.SupporterCategories.Add(supportercategories);
            db.SaveChanges();

            if (DataId == 0)
            {
                var supportercategories2 = db.SupporterCategories.Where(x => x.Id == supportercategories.Id).FirstOrDefault();
                supportercategories2.DataId = supportercategories.Id;
                db.SaveChanges();
            }
        }

        public static void SupporterCategoriesUpdate(string Id, int DataId, int LanguageId, int TypeId, string Title, string Content, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var supportercategories = db.SupporterCategories.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (supportercategories != null)
            {
                supportercategories.DataId = DataId;
                supportercategories.TypeId = TypeId;
                supportercategories.Title = Title;
                supportercategories.Content = Content;
                supportercategories.UpdatedDate = DateTime.Now;
                supportercategories.Owner = Owner;
                supportercategories.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string SupporterCategoriesDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.SupporterCategories.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool SupporterCategoriesIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var supportercategoriesUpdate = db.SupporterCategories.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            supportercategoriesUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool SupporterCategoriesSort(List<int> ids, int LanguageId, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.SupporterCategories.Where(x => x.LanguageId == LanguageId && x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //SupporterCategories End

        //Supporters Start
        public List<Supporters> GetSupporters()
        {
            var list = db.Supporters.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void SupportersSave(int DataId, int LanguageId, int ParentId, string Title, string Image, string Url, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var supporterss = new Supporters();
            if (DataId != 0) supporterss.DataId = DataId;
            supporterss.LanguageId = LanguageId;
            supporterss.ParentId = ParentId;
            supporterss.Title = Title;
            supporterss.Image = Image;
            supporterss.Url = Url;
            supporterss.Order = db.Supporters.Count() + 1;
            supporterss.CreatedDate = DateTime.Now;
            supporterss.Owner = Owner;
            supporterss.Active = DataActive;
            supporterss.Deleted = false;
            db.Supporters.Add(supporterss);
            db.SaveChanges();

            if (DataId == 0)
            {
                var supporterss2 = db.Supporters.Where(x => x.Id == supporterss.Id).FirstOrDefault();
                supporterss2.DataId = supporterss.Id;
                db.SaveChanges();
            }
        }

        public static void SupportersUpdate(string Id, int DataId, int LanguageId, int ParentId, string Title, string Image, string Url, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var supporterss = db.Supporters.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (supporterss != null)
            {
                supporterss.DataId = DataId;
                supporterss.ParentId = ParentId;
                supporterss.Title = Title;
                supporterss.Image = Image;
                supporterss.Url = Url;
                supporterss.UpdatedDate = DateTime.Now;
                supporterss.Owner = Owner;
                supporterss.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string SupportersDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Supporters.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool SupportersIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var supporterssUpdate = db.Supporters.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            supporterssUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool SupportersSort(List<int> ids, int LanguageId, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.Supporters.Where(x => x.LanguageId == LanguageId && x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //Supporters End

        //EventCategories Start
        public List<EventCategories> GetEventCategories()
        {
            var list = db.EventCategories.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void EventCategoriesSave(int DataId, int LanguageId, int ParentId, string PageFriendlyUrl, string Title, string ShortContent, string FriendlyUrl, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var eventcategories = new EventCategories();
            if (DataId != 0) eventcategories.DataId = DataId;
            eventcategories.LanguageId = LanguageId;
            eventcategories.ParentId = ParentId;
            eventcategories.PageFriendlyUrl = PageFriendlyUrl;
            eventcategories.Title = Title;
            eventcategories.ShortContent = ShortContent;
            eventcategories.Order = db.EventCategories.Count() + 1;
            eventcategories.CreatedDate = DateTime.Now;
            eventcategories.Owner = Owner;
            eventcategories.Active = DataActive;
            eventcategories.Deleted = false;

            //Friendly Url
            var ParentData = db.EventCategories.Where(x => x.DataId == ParentId && x.LanguageId == LanguageId).FirstOrDefault();

            if (FriendlyUrl == string.Empty)
            {
                if (ParentData != null)
                {
                    FriendlyUrl = ParentData.Title + " " + Title;
                }
                else
                {
                    FriendlyUrl = Title;
                }
            }

            var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            int HaveUrl = db.EventCategories.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

            if (HaveUrl == 0)
            {
                eventcategories.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            }
            else
            {
                for (int i = 0; i < 999; i++)
                {
                    HaveUrl = db.EventCategories.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                    if (HaveUrl == 0)
                    {
                        eventcategories.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                        break;
                    }
                }
            }
            //Friendly Url

            db.EventCategories.Add(eventcategories);
            db.SaveChanges();

            if (DataId == 0)
            {
                var eventcategories2 = db.EventCategories.Where(x => x.Id == eventcategories.Id).FirstOrDefault();
                eventcategories2.DataId = eventcategories.Id;
                db.SaveChanges();
            }
        }

        public static void EventCategoriesUpdate(string Id, int DataId, int LanguageId, int ParentId, string PageFriendlyUrl, string Title, string ShortContent, string FriendlyUrl, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var eventcategories = db.EventCategories.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (eventcategories != null)
            {
                eventcategories.DataId = DataId;
                eventcategories.ParentId = ParentId;
                eventcategories.PageFriendlyUrl = PageFriendlyUrl;
                eventcategories.Title = Title;
                eventcategories.ShortContent = ShortContent;
                eventcategories.UpdatedDate = DateTime.Now;
                eventcategories.Owner = Owner;
                eventcategories.Active = DataActive;

                //Friendly Url
                var ParentData = db.EventCategories.Where(x => x.DataId == ParentId && x.LanguageId == LanguageId).FirstOrDefault();

                if (FriendlyUrl == string.Empty)
                {
                    if (ParentData != null)
                    {
                        FriendlyUrl = ParentData.Title + " " + Title;
                    }
                    else
                    {
                        FriendlyUrl = Title;
                    }
                }

                var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                int HaveUrl = db.EventCategories.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

                if (HaveUrl == 0)
                {
                    eventcategories.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                }
                else
                {
                    for (int i = 0; i < 999; i++)
                    {
                        HaveUrl = db.EventCategories.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                        if (HaveUrl == 0)
                        {
                            eventcategories.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                            break;
                        }
                    }
                }
                //Friendly Url

                db.SaveChanges();
            }
        }

        public static string EventCategoriesDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.EventCategories.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool EventCategoriesIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var eventcategoriesUpdate = db.EventCategories.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            eventcategoriesUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool EventCategoriesSort(List<int> ids, int LanguageId, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.EventCategories.Where(x => x.LanguageId == LanguageId && x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //EventCategories End

        //Events Start
        public List<Events> GetEvents()
        {
            var list = db.Events.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void EventsSave(int DataId, int LanguageId, int ParentId, int PlaceId, string Title, string ShortContent, string Content, string Thumbnail, string Image, string Leaders, string Participants, DateTime StartDate, TimeSpan StartTime, DateTime EndDate, int FormId, string FriendlyUrl, string Keywords, string Description, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var events = new Events();
            if (DataId != 0) events.DataId = DataId;
            events.LanguageId = LanguageId;
            events.ParentId = ParentId;
            events.PlaceId = PlaceId;
            events.Title = Title;
            events.ShortContent = ShortContent;
            events.Content = Content;
            events.Thumbnail = Thumbnail;
            events.Image = Image;
            events.Leaders = Leaders;
            events.Participants = Participants;
            events.StartDate = StartDate;
            events.StartTime = StartTime;
            events.EndDate = EndDate;
            events.FormId = FormId;
            events.Keywords = Keywords;
            events.Description = Description;
            events.CreatedDate = DateTime.Now;
            events.Owner = Owner;
            events.Active = DataActive;
            events.Deleted = false;

            //Friendly Url
            var ParentData = db.EventCategories.Where(x => x.DataId == ParentId && x.LanguageId == LanguageId).FirstOrDefault();

            if (FriendlyUrl == string.Empty)
            {
                if (ParentData != null)
                {
                    FriendlyUrl = ParentData.Title + " " + Title;
                }
                else
                {
                    FriendlyUrl = Title;
                }
            }

            var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            int HaveUrl = db.Events.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

            if (HaveUrl == 0)
            {
                events.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            }
            else
            {
                for (int i = 0; i < 999; i++)
                {
                    HaveUrl = db.Events.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                    if (HaveUrl == 0)
                    {
                        events.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                        break;
                    }
                }
            }
            //Friendly Url

            db.Events.Add(events);
            db.SaveChanges();

            if (DataId == 0)
            {
                var events2 = db.Events.Where(x => x.Id == events.Id).FirstOrDefault();
                events2.DataId = events.Id;
                db.SaveChanges();
            }
        }

        public static void EventsUpdate(string Id, int DataId, int LanguageId, int ParentId, int PlaceId, string Title, string ShortContent, string Content, string Thumbnail, string Image, string Leaders, string Participants, DateTime StartDate, TimeSpan StartTime, DateTime EndDate, int FormId, string FriendlyUrl, string Keywords, string Description, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var events = db.Events.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (events != null)
            {
                events.DataId = DataId;
                events.ParentId = ParentId;
                events.PlaceId = PlaceId;
                events.Title = Title;
                events.ShortContent = ShortContent;
                events.Content = Content;
                events.Thumbnail = Thumbnail;
                events.Image = Image;
                events.Leaders = Leaders;
                events.Participants = Participants;
                events.StartDate = StartDate;
                events.StartTime = StartTime;
                events.EndDate = EndDate;
                events.FormId = FormId;
                events.Keywords = Keywords;
                events.Description = Description;
                events.UpdatedDate = DateTime.Now;
                events.Owner = Owner;
                events.Active = DataActive;

                //Friendly Url
                var ParentData = db.EventCategories.Where(x => x.DataId == ParentId && x.LanguageId == LanguageId).FirstOrDefault();

                if (FriendlyUrl == string.Empty)
                {
                    if (ParentData != null)
                    {
                        FriendlyUrl = ParentData.Title + " " + Title;
                    }
                    else
                    {
                        FriendlyUrl = Title;
                    }
                }

                var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                int HaveUrl = db.Events.Where(x => x.DataId != ConvertId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

                if (HaveUrl == 0)
                {
                    events.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                }
                else
                {
                    for (int i = 0; i < 999; i++)
                    {
                        HaveUrl = db.Events.Where(x => x.DataId != ConvertId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                        if (HaveUrl == 0)
                        {
                            events.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                            break;
                        }
                    }
                }
                //Friendly Url

                db.SaveChanges();
            }
        }

        public static string EventsDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Events.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool EventsIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var eventsUpdate = db.Events.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            eventsUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }
        //Events End

        //Press Start
        public List<Press> GetPress()
        {
            var list = db.Press.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void PressSave(int DataId, int LanguageId, int ParentId, string Title, string ShortContent, string Content, string Thumbnail, string Image, string Galleries, string Video, string File, string StartDate, string StartTime, string EndDate, string Link, string FriendlyUrl, string Keywords, string Description, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var press = new Press();
            if (DataId != 0) press.DataId = DataId;
            press.LanguageId = LanguageId;
            press.ParentId = ParentId;
            press.Title = Title;
            press.ShortContent = ShortContent;
            press.Content = Content;
            press.Thumbnail = Thumbnail;
            press.Image = Image;
            press.Galleries = Galleries;
            press.Video = Video;
            press.File = File;
            if (StartDate.ToString() != string.Empty) press.StartDate = Convert.ToDateTime(StartDate);
            if (StartTime.ToString() != string.Empty) press.StartTime = TimeSpan.Parse(StartTime);
            if (EndDate.ToString() != string.Empty) press.EndDate = Convert.ToDateTime(EndDate);
            press.Link = Link;
            press.Keywords = Keywords;
            press.Description = Description;
            press.Order = db.Press.Count() + 1;
            press.CreatedDate = DateTime.Now;
            press.Owner = Owner;
            press.Active = DataActive;
            press.Deleted = false;

            //Friendly Url
            if (FriendlyUrl == string.Empty)
            {
                FriendlyUrl = Title;
            }

            var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            int HaveUrl = db.Press.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

            if (HaveUrl == 0)
            {
                press.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            }
            else
            {
                for (int i = 0; i < 999; i++)
                {
                    HaveUrl = db.Press.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                    if (HaveUrl == 0)
                    {
                        press.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                        break;
                    }
                }
            }
            //Friendly Url

            db.Press.Add(press);
            db.SaveChanges();

            if (DataId == 0)
            {
                var press2 = db.Press.Where(x => x.Id == press.Id).FirstOrDefault();
                press2.DataId = press.Id;
                db.SaveChanges();
            }
        }

        public static void PressUpdate(string Id, int DataId, int LanguageId, int ParentId, string Title, string ShortContent, string Content, string Thumbnail, string Image, string Galleries, string Video, string File, string StartDate, string StartTime, string EndDate, string Link, string FriendlyUrl, string Keywords, string Description, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var press = db.Press.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (press != null)
            {
                press.DataId = DataId;
                press.ParentId = ParentId;
                press.Title = Title;
                press.ShortContent = ShortContent;
                press.Content = Content;
                press.Thumbnail = Thumbnail;
                press.Image = Image;
                press.Galleries = Galleries;
                press.Video = Video;
                press.File = File;
                press.StartDate = StartDate.ToString() != string.Empty ? Convert.ToDateTime(StartDate) : Convert.ToDateTime(ExtensionMethod.NullDate);
                if (StartTime.ToString() != string.Empty) press.StartTime = TimeSpan.Parse(StartTime);
                press.EndDate = EndDate.ToString() != string.Empty ? Convert.ToDateTime(EndDate) : Convert.ToDateTime(ExtensionMethod.NullDate);
                press.Link = Link;
                press.Keywords = Keywords;
                press.Description = Description;
                press.UpdatedDate = DateTime.Now;
                press.Owner = Owner;
                press.Active = DataActive;

                //Friendly Url
                if (FriendlyUrl == string.Empty)
                {
                    FriendlyUrl = Title;
                }

                var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                int HaveUrl = db.Press.Where(x => x.DataId != ConvertId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

                if (HaveUrl == 0)
                {
                    press.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                }
                else
                {
                    for (int i = 0; i < 999; i++)
                    {
                        HaveUrl = db.Press.Where(x => x.DataId != ConvertId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                        if (HaveUrl == 0)
                        {
                            press.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                            break;
                        }
                    }
                }
                //Friendly Url

                db.SaveChanges();
            }
        }

        public static string PressDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Press.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool PressIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var pressUpdate = db.Press.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            pressUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool PressSort(List<int> ids, int LanguageId, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.Press.Where(x => x.LanguageId == LanguageId && x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //Press End

        //Newsletter Start
        public List<Newsletter> GetNewsletter()
        {
            var list = db.Newsletter.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static string NewsletterDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Newsletter.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool NewsletterIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var newsletterUpdate = db.Newsletter.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            newsletterUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }
        //Newsletter End

        //Translates Start
        public List<Translates> GetTranslates()
        {
            var list = db.Translates.ToList();
            return list;
        }

        public static void TranslatesSave(int DataId, int LanguageId, string Description, string Text, int Owner, object login, DatabaseConnection db)
        {
            var translates = new Translates();
            if (DataId != 0) translates.DataId = DataId;
            translates.LanguageId = LanguageId;
            translates.Description = Description;
            translates.Text = Text;
            translates.CreatedDate = DateTime.Now;
            translates.Owner = Owner;
            db.Translates.Add(translates);
            db.SaveChanges();

            if (DataId == 0)
            {
                var translates2 = db.Translates.Where(x => x.Id == translates.Id).FirstOrDefault();
                translates2.DataId = translates.Id;
                db.SaveChanges();
            }
        }

        public static void TranslatesUpdate(string Id, int DataId, int LanguageId, string Description, string Text, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();

            var translates = db.Translates.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId).FirstOrDefault();
            if (translates != null)
            {
                translates.DataId = DataId;
                translates.Description = Description;
                translates.Text = Text;
                translates.UpdatedDate = DateTime.Now;
                translates.Owner = Owner;
                db.SaveChanges();
            }
        }
        //Translates End

        //ContactForm Start
        public List<ContactForm> GetContactForm()
        {
            var list = db.ContactForm.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static string ContactFormDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.ContactForm.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool ContactFormIsRead(string Id, string Read, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var contactformUpdate = db.ContactForm.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            contactformUpdate.Read = Convert.ToBoolean(Read);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }
        //ContactForm End

        //WorkshopForm Start
        public List<WorkshopForm> GetWorkshopForm()
        {
            var list = db.WorkshopForm.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static string WorkshopFormDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.WorkshopForm.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool WorkshopFormIsRead(string Id, string Read, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var workshopformUpdate = db.WorkshopForm.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            workshopformUpdate.Read = Convert.ToBoolean(Read);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }
        //WorkshopForm End

        //VolunteerForm Start
        public List<VolunteerForm> GetVolunteerForm()
        {
            var list = db.VolunteerForm.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static string VolunteerFormDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.VolunteerForm.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool VolunteerFormIsRead(string Id, string Read, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var volunteerformUpdate = db.VolunteerForm.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            volunteerformUpdate.Read = Convert.ToBoolean(Read);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }
        //VolunteerForm End

        /////////////// <- Panel | Site -> ///////////////

        public List<Pages> SiteGetPages(int SiteLanguage)
        {
            var list = db.Pages.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<Peoples> SiteGetPeoples(int SiteLanguage)
        {
            var list = db.Peoples.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<PeopleCategories> SiteGetPeopleCategories(int SiteLanguage)
        {
            var list = db.PeopleCategories.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<Supporters> SiteGetSupporters(int SiteLanguage)
        {
            var list = db.Supporters.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<SupporterCategories> SiteGetSupporterCategories(int SiteLanguage)
        {
            var list = db.SupporterCategories.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<Press> SiteGetPress(int SiteLanguage)
        {
            var list = db.Press.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }
        public List<GalleryParentCategories> SiteGetGalleryParentCategories(int SiteLanguage)
        {
            var list = db.GalleryParentCategories.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<GalleryCategories> SiteGetGalleryCategories(int SiteLanguage)
        {
            var list = db.GalleryCategories.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<Gallery> SiteGetGallery(int SiteLanguage)
        {
            var list = db.Gallery.OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<EventCategories> SiteGetEventCategories(int SiteLanguage)
        {
            var list = db.EventCategories.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<Events> SiteGetEvents(int SiteLanguage)
        {
            var list = db.Events.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderByDescending(x => x.StartDate).ThenBy(x => x.EndDate).ToList();
            return list;
        }

        public List<SocialMedia> SiteGetSocialMedia()
        {
            var list = db.SocialMedia.Where(x => x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<Languages> SiteGetAllLanguages()
        {
            var list = db.Languages.Where(x => x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public Languages SiteGetLanguage(string ShortName)
        {
            var single = db.Languages.Where(x => x.ShortName == ShortName).FirstOrDefault();
            return single;
        }

        public List<Translates> SiteGetAllTranslates(int SiteLanguage)
        {
            var list = db.Translates.Where(x => x.LanguageId == SiteLanguage).ToList();
            return list;
        }

        public List<MultiPreferences> SiteGetMultiPreferences(int SiteLanguage)
        {
            var list = db.MultiPreferences.Where(x => x.LanguageId == SiteLanguage).ToList();
            return list;
        }

        public static void ContactFormSave(string Name, string Surname, string Email, string Phone, string Subject, string Message, DatabaseConnection db)
        {
            var contactform = new ContactForm();
            contactform.Name = Name;
            contactform.Surname = Surname;
            contactform.Email = Email;
            contactform.Phone = Phone;
            contactform.Subject = Subject;
            contactform.Message = Message;
            contactform.CreatedDate = DateTime.Now;
            contactform.Read = false;
            contactform.Deleted = false;
            db.ContactForm.Add(contactform);
            db.SaveChanges();
        }

        public static void WorkshopFormSave(string Name, string Surname, string Birthday, string Gender, string IdentificationNumber, string Email, string Phone, string Country, string City, string District, string PostalCode, string Address, string Sector, string EducationDegree, string GraduatedSchool, string GraduatedSchoolSection, string CurrentSchool, string CurrentSchoolSection, string Program, string Informed, string WebSite, string Cv, string Allergy, string Nutrition, string RelationNameSurname, string RelationProximity, string RelationPhone, DatabaseConnection db)
        {
            var workshopform = new WorkshopForm();
            workshopform.Name = Name;
            workshopform.Surname = Surname;
            workshopform.Birthday = Birthday;
            workshopform.Gender = Gender;
            workshopform.IdentificationNumber = IdentificationNumber;
            workshopform.Email = Email;
            workshopform.Phone = Phone;
            workshopform.Country = Country;
            workshopform.City = City;
            workshopform.District = District;
            workshopform.PostalCode = PostalCode;
            workshopform.Address = Address;
            workshopform.Sector = Sector;
            workshopform.EducationDegree = EducationDegree;
            workshopform.GraduatedSchool = GraduatedSchool;
            workshopform.GraduatedSchoolSection = GraduatedSchoolSection;
            workshopform.CurrentSchool = CurrentSchool;
            workshopform.CurrentSchoolSection = CurrentSchoolSection;
            workshopform.Program = Program;
            workshopform.Informed = Informed;
            workshopform.WebSite = WebSite;
            workshopform.Cv = Cv;
            workshopform.Allergy = Allergy;
            workshopform.Nutrition = Nutrition;
            workshopform.RelationNameSurname = RelationNameSurname;
            workshopform.RelationProximity = RelationProximity;
            workshopform.RelationPhone = RelationPhone;
            workshopform.CreatedDate = DateTime.Now;
            workshopform.Deleted = false;
            db.WorkshopForm.Add(workshopform);
            db.SaveChanges();
        }

        public static void VolunteerFormSave(string Name, string Surname, string Birthday, string Gender, string Email, string Phone, string Country, string City, string District, string Sector, string EducationDegree, string GraduatedSchool, string GraduatedSchoolSection, string CurrentSchool, string CurrentSchoolSection, string Program, string Informed, string Cv, string HowToSupport, string SupportTypes, string WorkingDate, string WorkingTime, string Day1, string Day2, string Day3, string Day4, string Day5, string Day6, string Day7, DatabaseConnection db)
        {
            var volunteerform = new VolunteerForm();
            volunteerform.Name = Name;
            volunteerform.Surname = Surname;
            volunteerform.Birthday = Birthday;
            volunteerform.Gender = Gender;
            volunteerform.Email = Email;
            volunteerform.Phone = Phone;
            volunteerform.Country = Country;
            volunteerform.City = City;
            volunteerform.District = District;
            volunteerform.Sector = Sector;
            volunteerform.EducationDegree = EducationDegree;
            volunteerform.GraduatedSchool = GraduatedSchool;
            volunteerform.GraduatedSchoolSection = GraduatedSchoolSection;
            volunteerform.CurrentSchool = CurrentSchool;
            volunteerform.CurrentSchoolSection = CurrentSchoolSection;
            volunteerform.Program = Program;
            volunteerform.Informed = Informed;
            volunteerform.Cv = Cv;
            volunteerform.HowToSupport = HowToSupport;
            volunteerform.SupportTypes = SupportTypes;
            volunteerform.WorkingDate = WorkingDate;
            volunteerform.WorkingTime = WorkingTime;
            volunteerform.Day1 = Day1;
            volunteerform.Day2 = Day2;
            volunteerform.Day3 = Day3;
            volunteerform.Day4 = Day4;
            volunteerform.Day5 = Day5;
            volunteerform.Day6 = Day6;
            volunteerform.Day7 = Day7;
            volunteerform.CreatedDate = DateTime.Now;
            volunteerform.Deleted = false;
            db.VolunteerForm.Add(volunteerform);
            db.SaveChanges();
        }

        public static void NewsletterSave(string Email, DatabaseConnection db)
        {
            var newsletterform = new Newsletter();
            newsletterform.Email = Email;
            newsletterform.Active = true;
            newsletterform.CreatedDate = DateTime.Now;
            newsletterform.Deleted = false;
            db.Newsletter.Add(newsletterform);
            db.SaveChanges();
        }

        public static bool IsMailExist(string email)
        {
            using (DatabaseConnection db = new DatabaseConnection())
            {
                return db.Newsletter.Any(p => p.Email == email);
            }
        }
    }

}
