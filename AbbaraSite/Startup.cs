﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AbbaraSite.Startup))]
namespace AbbaraSite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
