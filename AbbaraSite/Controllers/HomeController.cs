﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL;
using DAL.Repo;
using System.Threading;
using System.Configuration;
using System.Globalization;

namespace AbbaraSite.Controllers
{
    public class LanguageActionFilter : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContextBase context = filterContext.RequestContext.HttpContext;
            string cookie = context.Request.Cookies["culture"] != null && context.Request.Cookies["culture"].Value != "" ? context.Request.Cookies["culture"].Value : ConfigurationManager.AppSettings["DefaultLanguage"];
            if (cookie != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(cookie);
                Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(cookie);
            }
        }
    }

    public class HomeController : Controller
    {
        private DatabaseConnection db = new DatabaseConnection();

        public void ChangeLanguage(string ShortName)
        {
            Response.Cookies.Remove("culture");
            Response.Cookies.Add(new HttpCookie("culture")
            {
                Name = "culture",
                Expires = DateTime.Now.AddDays(3),
                Path = "/",
                Value = ShortName
            });

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            int oldLang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            int newLang = main.SiteGetLanguage(ShortName).Id;
            Uri refUrl = Request.UrlReferrer;
            string refUrl2 = refUrl != null ? refUrl.Host : "";
            string curUrl = Request.Url.Host;
            string friendlyUrl;
            string subFriendlyUrl;

            if (refUrl2 != curUrl)
            {
                Response.Redirect("/");
            }
            else if (refUrl.Segments.Count() <= 1)
            {
                Response.Redirect("/");
            }
            else
            {
                if (refUrl.Segments[1].ToLower() == "sayfa/" || refUrl.Segments[1].ToLower() == "calendar/" || refUrl.Segments[1].ToLower() == "visuals/")
                {
                    friendlyUrl = refUrl.Segments[2];
                    subFriendlyUrl = refUrl.Segments.Count() > 3 ? "/" + refUrl.Segments[3] : string.Empty;
                    main.Pages = main.SiteGetPages(oldLang);
                    var oldData = main.Pages.Where(x => x.FriendlyUrl == friendlyUrl).FirstOrDefault();
                    main.Pages = main.SiteGetPages(newLang);
                    var newData = main.Pages.Where(x => x.DataId == oldData.DataId).FirstOrDefault();
                    Response.Redirect(newData == null ? "/" : "/" + refUrl.Segments[1] + newData.FriendlyUrl + subFriendlyUrl);
                }
                else if (refUrl.Segments[1].ToLower() == "peoples/")
                {
                    if (refUrl.Segments.Count() == 3)
                    {
                        friendlyUrl = refUrl.Segments[2];
                        main.PeopleCategories = main.SiteGetPeopleCategories(oldLang);
                        var oldData = main.PeopleCategories.Where(x => x.PageFriendlyUrl == friendlyUrl).FirstOrDefault();
                        main.PeopleCategories = main.SiteGetPeopleCategories(newLang);
                        var newData = main.PeopleCategories.Where(x => x.DataId == oldData.DataId).FirstOrDefault();
                        Response.Redirect(newData == null ? "/" : "/" + refUrl.Segments[1] + newData.PageFriendlyUrl);
                    }
                    else if (refUrl.Segments.Count() == 4)
                    {
                        friendlyUrl = refUrl.Segments[2].Replace("/", "");
                        subFriendlyUrl = refUrl.Segments[3].Replace("/", "");
                        main.Peoples = main.SiteGetPeoples(oldLang);
                        main.PeopleCategories = main.SiteGetPeopleCategories(oldLang);
                        var oldData = main.Peoples.Where(x => x.FriendlyUrl == subFriendlyUrl).FirstOrDefault();
                        var oldData2 = main.PeopleCategories.Where(x => x.PageFriendlyUrl == friendlyUrl).FirstOrDefault();
                        main.Peoples = main.SiteGetPeoples(newLang);
                        main.PeopleCategories = main.SiteGetPeopleCategories(newLang);
                        var newData = main.Peoples.Where(x => x.DataId == oldData.DataId).FirstOrDefault();
                        var newData2 = main.PeopleCategories.Where(x => x.DataId == oldData2.DataId).FirstOrDefault();
                        Response.Redirect(newData == null ? "/" : "/" + refUrl.Segments[1] + newData2.PageFriendlyUrl + "/" + newData.FriendlyUrl);
                    }
                }
                else if (refUrl.Segments[1].ToLower() == "announcements/" || refUrl.Segments[1].ToLower() == "press/" || refUrl.Segments[1].ToLower() == "presskit/")
                {
                    if (refUrl.Segments.Count() == 3)
                    {
                        friendlyUrl = refUrl.Segments[2];
                        main.Pages = main.SiteGetPages(oldLang);
                        var oldData = main.Pages.Where(x => x.FriendlyUrl == friendlyUrl).FirstOrDefault();
                        main.Pages = main.SiteGetPages(newLang);
                        var newData = main.Pages.Where(x => x.DataId == oldData.DataId).FirstOrDefault();
                        Response.Redirect(newData == null ? "/" : "/" + refUrl.Segments[1] + newData.FriendlyUrl);
                    }
                    else if (refUrl.Segments.Count() == 4)
                    {
                        friendlyUrl = refUrl.Segments[2].Replace("/", "");
                        subFriendlyUrl = refUrl.Segments[3].Replace("/", "");
                        main.Press = main.SiteGetPress(oldLang);
                        main.Pages = main.SiteGetPages(oldLang);
                        var oldData = main.Press.Where(x => x.FriendlyUrl == subFriendlyUrl).FirstOrDefault();
                        var oldData2 = main.Pages.Where(x => x.FriendlyUrl == friendlyUrl).FirstOrDefault();
                        main.Press = main.SiteGetPress(newLang);
                        main.Pages = main.SiteGetPages(newLang);
                        var newData = main.Press.Where(x => x.DataId == oldData.DataId).FirstOrDefault();
                        var newData2 = main.Pages.Where(x => x.DataId == oldData2.DataId).FirstOrDefault();
                        Response.Redirect(newData == null ? "/" : "/" + refUrl.Segments[1] + newData2.FriendlyUrl + "/" + newData.FriendlyUrl);
                    }
                }
                else if (refUrl.Segments[1].ToLower() == "program/")
                {
                    if (refUrl.Segments.Count() == 5)
                    {
                        friendlyUrl = refUrl.Segments[4];
                        main.Pages = main.SiteGetPages(oldLang);
                        var oldData = main.Pages.Where(x => x.FriendlyUrl == friendlyUrl).FirstOrDefault();
                        main.Pages = main.SiteGetPages(newLang);
                        var newData = main.Pages.Where(x => x.DataId == oldData.DataId).FirstOrDefault();
                        Response.Redirect(newData == null ? "/" : "/" + newData.Link + "/" + newData.FriendlyUrl);
                    }
                    else if (refUrl.Segments.Count() == 6)
                    {
                        friendlyUrl = refUrl.Segments[4].Replace("/", "");
                        subFriendlyUrl = refUrl.Segments[5].Replace("/", "");
                        main.Events = main.SiteGetEvents(oldLang);
                        main.Pages = main.SiteGetPages(oldLang);
                        var oldData = main.Events.Where(x => x.FriendlyUrl == subFriendlyUrl).FirstOrDefault();
                        var oldData2 = main.Pages.Where(x => x.FriendlyUrl == friendlyUrl).FirstOrDefault();
                        main.Events = main.SiteGetEvents(newLang);
                        main.Pages = main.SiteGetPages(newLang);
                        var newData = main.Events.Where(x => x.DataId == oldData.DataId).FirstOrDefault();
                        var newData2 = main.Pages.Where(x => x.DataId == oldData2.DataId).FirstOrDefault();
                        Response.Redirect(newData == null ? "/" : "/" + newData2.Link + "/" + newData2.FriendlyUrl + "/" + newData.FriendlyUrl);
                    }
                }
                else if (refUrl.Segments[1].ToLower() == "calendarlist/")
                {
                    friendlyUrl = refUrl.Segments[4];
                    main.Pages = main.SiteGetPages(oldLang);
                    var oldData = main.Pages.Where(x => x.FriendlyUrl == friendlyUrl).FirstOrDefault();
                    main.Pages = main.SiteGetPages(newLang);
                    var newData = main.Pages.Where(x => x.DataId == oldData.DataId).FirstOrDefault();
                    Response.Redirect(newData == null ? "/" : "/" + refUrl.Segments[1] + "/" + refUrl.Segments[2] + "/" + refUrl.Segments[3] + newData.FriendlyUrl);
                }
                else
                {
                    Response.Redirect("/");
                }
            }
        }

        public ActionResult Index()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            ViewBag.lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.Translates = main.SiteGetAllTranslates(ViewBag.lang);
            main.MultiPreferences = main.SiteGetMultiPreferences(ViewBag.lang);
            main.Languages = main.SiteGetAllLanguages();
            main.Pages = main.SiteGetPages(ViewBag.lang);
            main.SocialMedia = main.SiteGetSocialMedia();
            return View(main);
        }

        public ActionResult _LeftMenu()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            ViewBag.lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.Translates = main.SiteGetAllTranslates(ViewBag.lang);
            main.MultiPreferences = main.SiteGetMultiPreferences(ViewBag.lang);
            main.Languages = main.SiteGetAllLanguages();
            main.Pages = main.SiteGetPages(ViewBag.lang);
            return View(main);
        }

        public ActionResult Sayfa()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            ViewBag.lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.Translates = main.SiteGetAllTranslates(ViewBag.lang);
            main.MultiPreferences = main.SiteGetMultiPreferences(ViewBag.lang);
            main.Languages = main.SiteGetAllLanguages();
            main.Pages = main.SiteGetPages(ViewBag.lang);
            main.SocialMedia = main.SiteGetSocialMedia();
            main.GalleryParentCategories = main.SiteGetGalleryParentCategories(ViewBag.lang);
            main.GalleryCategories = main.SiteGetGalleryCategories(ViewBag.lang);
            main.Gallery = main.SiteGetGallery(ViewBag.lang);
            return View(main);
        }

        public ActionResult Peoples()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            ViewBag.lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.Translates = main.SiteGetAllTranslates(ViewBag.lang);
            main.MultiPreferences = main.SiteGetMultiPreferences(ViewBag.lang);
            main.Languages = main.SiteGetAllLanguages();
            main.Pages = main.SiteGetPages(ViewBag.lang);
            main.SocialMedia = main.SiteGetSocialMedia();
            main.Peoples = main.SiteGetPeoples(ViewBag.lang);
            main.PeopleCategories = main.SiteGetPeopleCategories(ViewBag.lang);
            return View(main);
        }

        public ActionResult PartnersAndCo()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            ViewBag.lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.Translates = main.SiteGetAllTranslates(ViewBag.lang);
            main.MultiPreferences = main.SiteGetMultiPreferences(ViewBag.lang);
            main.Languages = main.SiteGetAllLanguages();
            main.Pages = main.SiteGetPages(ViewBag.lang);
            main.SocialMedia = main.SiteGetSocialMedia();
            main.Supporters = main.SiteGetSupporters(ViewBag.lang);
            main.SupporterCategories = main.SiteGetSupporterCategories(ViewBag.lang);
            return View(main);
        }

        public ActionResult Sponsorship()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            ViewBag.lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.Translates = main.SiteGetAllTranslates(ViewBag.lang);
            main.MultiPreferences = main.SiteGetMultiPreferences(ViewBag.lang);
            main.Languages = main.SiteGetAllLanguages();
            main.Pages = main.SiteGetPages(ViewBag.lang);
            main.SocialMedia = main.SiteGetSocialMedia();
            main.Supporters = main.SiteGetSupporters(ViewBag.lang);
            main.SupporterCategories = main.SiteGetSupporterCategories(ViewBag.lang);
            return View(main);
        }

        public ActionResult Announcements()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            ViewBag.lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.Translates = main.SiteGetAllTranslates(ViewBag.lang);
            main.MultiPreferences = main.SiteGetMultiPreferences(ViewBag.lang);
            main.Languages = main.SiteGetAllLanguages();
            main.Pages = main.SiteGetPages(ViewBag.lang);
            main.SocialMedia = main.SiteGetSocialMedia();
            main.GalleryCategories = main.SiteGetGalleryCategories(ViewBag.lang);
            main.Gallery = main.SiteGetGallery(ViewBag.lang);
            main.Press = main.SiteGetPress(ViewBag.lang);
            return View(main);
        }

        public ActionResult Press()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            ViewBag.lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.Translates = main.SiteGetAllTranslates(ViewBag.lang);
            main.MultiPreferences = main.SiteGetMultiPreferences(ViewBag.lang);
            main.Languages = main.SiteGetAllLanguages();
            main.Pages = main.SiteGetPages(ViewBag.lang);
            main.SocialMedia = main.SiteGetSocialMedia();
            main.Press = main.SiteGetPress(ViewBag.lang);
            return View(main);
        }

        public ActionResult PressKit()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            ViewBag.lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.Translates = main.SiteGetAllTranslates(ViewBag.lang);
            main.MultiPreferences = main.SiteGetMultiPreferences(ViewBag.lang);
            main.Languages = main.SiteGetAllLanguages();
            main.Pages = main.SiteGetPages(ViewBag.lang);
            main.SocialMedia = main.SiteGetSocialMedia();
            main.GalleryCategories = main.SiteGetGalleryCategories(ViewBag.lang);
            main.Gallery = main.SiteGetGallery(ViewBag.lang);
            main.Press = main.SiteGetPress(ViewBag.lang);
            return View(main);
        }

        public ActionResult PressReview()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            ViewBag.lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.Translates = main.SiteGetAllTranslates(ViewBag.lang);
            main.MultiPreferences = main.SiteGetMultiPreferences(ViewBag.lang);
            main.Languages = main.SiteGetAllLanguages();
            main.Pages = main.SiteGetPages(ViewBag.lang);
            main.SocialMedia = main.SiteGetSocialMedia();
            main.Press = main.SiteGetPress(ViewBag.lang);
            return View(main);
        }

        public ActionResult Visuals()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            ViewBag.lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.Translates = main.SiteGetAllTranslates(ViewBag.lang);
            main.MultiPreferences = main.SiteGetMultiPreferences(ViewBag.lang);
            main.Languages = main.SiteGetAllLanguages();
            main.Pages = main.SiteGetPages(ViewBag.lang);
            main.SocialMedia = main.SiteGetSocialMedia();
            main.GalleryParentCategories = main.SiteGetGalleryParentCategories(ViewBag.lang);
            main.GalleryCategories = main.SiteGetGalleryCategories(ViewBag.lang);
            main.Gallery = main.SiteGetGallery(ViewBag.lang);
            return View(main);
        }

        public ActionResult Program()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            ViewBag.lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.Translates = main.SiteGetAllTranslates(ViewBag.lang);
            main.MultiPreferences = main.SiteGetMultiPreferences(ViewBag.lang);
            main.Languages = main.SiteGetAllLanguages();
            main.Pages = main.SiteGetPages(ViewBag.lang);
            main.SocialMedia = main.SiteGetSocialMedia();
            main.EventCategories = main.SiteGetEventCategories(ViewBag.lang);
            main.Events = main.SiteGetEvents(ViewBag.lang);
            return View(main);
        }

        public ActionResult Calendar()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            ViewBag.lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.Translates = main.SiteGetAllTranslates(ViewBag.lang);
            main.MultiPreferences = main.SiteGetMultiPreferences(ViewBag.lang);
            main.Languages = main.SiteGetAllLanguages();
            main.Pages = main.SiteGetPages(ViewBag.lang);
            main.SocialMedia = main.SiteGetSocialMedia();
            main.EventCategories = main.SiteGetEventCategories(ViewBag.lang);
            main.Events = main.SiteGetEvents(ViewBag.lang);
            return View(main);
        }

        public ActionResult CalendarList()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            ViewBag.lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.Translates = main.SiteGetAllTranslates(ViewBag.lang);
            main.MultiPreferences = main.SiteGetMultiPreferences(ViewBag.lang);
            main.Languages = main.SiteGetAllLanguages();
            main.Pages = main.SiteGetPages(ViewBag.lang);
            main.SocialMedia = main.SiteGetSocialMedia();
            main.EventCategories = main.SiteGetEventCategories(ViewBag.lang);
            main.Events = main.SiteGetEvents(ViewBag.lang);
            return View(main);
        }

        public ActionResult WorkshopForm()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            ViewBag.lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.Translates = main.SiteGetAllTranslates(ViewBag.lang);
            main.MultiPreferences = main.SiteGetMultiPreferences(ViewBag.lang);
            main.Languages = main.SiteGetAllLanguages();
            main.Pages = main.SiteGetPages(ViewBag.lang);
            main.SocialMedia = main.SiteGetSocialMedia();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult WorkshopFormSave(FormCollection fc)
        {
            AdminMainProgress.WorkshopFormSave(fc["Name"], fc["Surname"], fc["Birthday"], fc["Gender"], fc["IdentificationNumber"], fc["Email"], fc["Phone"], fc["Country"], fc["City"], fc["District"], fc["PostalCode"], fc["Address"], fc["Sector"], fc["EducationDegree"], fc["GraduatedSchool"], fc["GraduatedSchoolSection"], fc["CurrentSchool"], fc["CurrentSchoolSection"], fc["Program"], fc["Informed"], fc["WebSite"], fc["Cv"], fc["Allergy"], fc["Nutrition"], fc["RelationNameSurname"], fc["RelationProximity"], fc["RelationPhone"], db);
            ViewBag.Result = true;
            Uri url = System.Web.HttpContext.Current.Request.UrlReferrer;
            return Content("<script language='javascript' type='text/javascript'>alert('Formunuz alındı, teşekkürler.');window.location = '" + url.ToString() + "';</script>");
        }

        public ActionResult VolunteerForm()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            ViewBag.lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.Translates = main.SiteGetAllTranslates(ViewBag.lang);
            main.MultiPreferences = main.SiteGetMultiPreferences(ViewBag.lang);
            main.Languages = main.SiteGetAllLanguages();
            main.Pages = main.SiteGetPages(ViewBag.lang);
            main.SocialMedia = main.SiteGetSocialMedia();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult VolunteerFormSave(FormCollection fc)
        {
            AdminMainProgress.VolunteerFormSave(fc["Name"], fc["Surname"], fc["Birthday"], fc["Gender"], fc["Email"], fc["Phone"], fc["Country"], fc["City"], fc["District"], fc["Sector"], fc["EducationDegree"], fc["GraduatedSchool"], fc["GraduatedSchoolSection"], fc["CurrentSchool"], fc["CurrentSchoolSection"], fc["Program"], fc["Informed"], fc["Cv"], fc["HowToSupport"], fc["SupportTypes"], fc["WorkingDate"], fc["WorkingTime"], fc["Day1"], fc["Day2"], fc["Day3"], fc["Day4"], fc["Day5"], fc["Day6"], fc["Day7"], db);
            ViewBag.Result = true;
            Uri url = System.Web.HttpContext.Current.Request.UrlReferrer;
            return Content("<script language='javascript' type='text/javascript'>alert('Formunuz alındı, teşekkürler.');window.location = '" + url.ToString() + "';</script>");
        }

        public ActionResult ContactForm()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            ViewBag.lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.Translates = main.SiteGetAllTranslates(ViewBag.lang);
            main.MultiPreferences = main.SiteGetMultiPreferences(ViewBag.lang);
            main.Languages = main.SiteGetAllLanguages();
            main.Pages = main.SiteGetPages(ViewBag.lang);
            main.SocialMedia = main.SiteGetSocialMedia();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ContactFormSave(FormCollection fc)
        {
            AdminMainProgress.ContactFormSave(fc["Name"], fc["Surname"], fc["Email"], fc["Phone"], fc["Subject"], fc["Message"], db);
            ViewBag.Result = true;
            Uri url = System.Web.HttpContext.Current.Request.UrlReferrer;
            return Content("<script language='javascript' type='text/javascript'>alert('" + fc["Result"] + "');window.location = '" + url.ToString() + "';</script>");
        }

        public ActionResult Search()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            ViewBag.lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.Translates = main.SiteGetAllTranslates(ViewBag.lang);
            main.MultiPreferences = main.SiteGetMultiPreferences(ViewBag.lang);
            main.Languages = main.SiteGetAllLanguages();
            main.Pages = main.SiteGetPages(ViewBag.lang);
            main.SocialMedia = main.SiteGetSocialMedia();
            main.Peoples = main.SiteGetPeoples(ViewBag.lang);
            main.PeopleCategories = main.SiteGetPeopleCategories(ViewBag.lang);
            main.Events = main.SiteGetEvents(ViewBag.lang);
            main.EventCategories = main.SiteGetEventCategories(ViewBag.lang);
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Search(FormCollection fc)
        {
            string text = fc["search"];
            return Redirect("/Search/" + text);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult NewsletterSave(FormCollection fc)
        {
            Uri url = System.Web.HttpContext.Current.Request.UrlReferrer;
            var isExist = AdminMainProgress.IsMailExist(fc["Email"]);
            if (isExist)
            {
                return Content("<script language='javascript' type='text/javascript'>alert('Bu e-posta adresi ile daha önce kayıt yapılmış.');window.location = '" + url.ToString() + "';</script>");
            }
            AdminMainProgress.NewsletterSave(fc["Email"], db);
            return Content("<script language='javascript' type='text/javascript'>alert('Bültene kaydınız yapıldı.');window.location = '" + url.ToString() + "';</script>");
        }

        [HttpGet]
        public JsonResult GetMonthNames()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            ViewBag.lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.Translates = main.SiteGetAllTranslates(ViewBag.lang);
            List<string> data = new List<string>();

            for (int i = 219; i < 231; i++)
            {
                data.Add(main.Translates.Where(x => x.DataId == i).FirstOrDefault().Text);
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}