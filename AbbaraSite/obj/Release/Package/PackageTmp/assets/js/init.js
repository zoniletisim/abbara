﻿/*NAVİGATE*/
jQuery(document).ready(function ($) {
    var smallWindow = false;

    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll >= 50) {
            $('#logo-image').attr('src', '/assets/img/ab_logo_small.png')
            $(".important-class").addClass("padding-on-my-header");
            $(".important-class2").addClass("padding-on-my-header2");
            $(".bar2").addClass("bar-2");

        }
        if (scroll < 50) {
            $(".important-class").removeClass("padding-on-my-header");
            $(".important-class2").removeClass("padding-on-my-header2");
            $(".bar2").addClass("bar-2");
            $('#logo-image').attr('src', '/assets/img/ab_logo.png')
        }
    }).resize(function () {
        if (!smallWindow && this.innerWidth <= 1024) {
            smallWindow = true;
            $('.top-bar-section').find('ul.right').hide(0).delay(400).show(0);
        }
        if (smallWindow && this.innerWidth > 1024) {
            smallWindow = false;
        }
    });


});


$(document).ready(function () {

    $("#nav ul.child").removeClass("child");

    $("#nav li").has("ul").hover(function () {
        $(this).addClass("current").children("ul").fadeIn();
    }, function () {
        $(this).removeClass("current").children("ul").hide();
    });

});

/*MENU*/
(function ($) {
    $(document).ready(function () {

        $('#cssmenu li.active').addClass('open').children('ul').show();
        $('#cssmenu li.has-sub>a').on('click', function () {
            $(this).removeAttr('href');
            var element = $(this).parent('li');
            if (element.hasClass('open')) {
                element.removeClass('open');
                element.find('li').removeClass('open');
                element.find('ul').slideUp(200);
            }
            else {
                element.addClass('open');
                element.children('ul').slideDown(200);
                element.siblings('li').children('ul').slideUp(200);
                element.siblings('li').removeClass('open');
                element.siblings('li').find('li').removeClass('open');
                element.siblings('li').find('ul').slideUp(200);
            }
        });

    });
})(jQuery);

$(document).ready(function () {
    var $lightbox = $('#lightbox');

    $('[data-target="#lightbox"]').on('click', function (event) {
        var $img = $(this).find('img'),
            src = $img.attr('src'),
            alt = $img.attr('alt'),
            css = {
                'maxWidth': $(window).width() - 100,
                'maxHeight': $(window).height() - 100
            };

        $lightbox.find('.close').addClass('hidden');
        $lightbox.find('img').attr('src', src);
        $lightbox.find('img').attr('alt', alt);
        $lightbox.find('img').css(css);
    });

    $lightbox.on('shown.bs.modal', function (e) {
        var $img = $lightbox.find('img');

        $lightbox.find('.modal-dialog').css({ 'width': $img.width() });
        $lightbox.find('.close').removeClass('hidden');
    });
});
$(document).ready(function () {
    var $box = $('.thumb').css('width');
    var $img = $('.thumb img').width();
    var $gox = (($img / 2 - (parseInt($box, 10) / 2)) + 'px');
    $('.thumb img').css('marginLeft', '-' + $gox);


    //Just the same code as above, but with window.onresize
    window.onresize = function (event) {
        var $box = $('.thumb').css('width');
        var $img = $('.thumb img').width();
        var $gox = (($img / 2 - (parseInt($box, 10) / 2)) + 'px');
        $('.thumb img').css('marginLeft', '-' + $gox);
    }
});

function initMenu() {
    $('#subMenu ul').hide();
    $('#subMenu li a').click(

    function () {

        var checkElement = $(this).next();
        if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
            $('#subMenu ul:visible').slideToggle('normal');
        }
        if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
            removeActiveClassFromAll();
            $(this).addClass("subMenuactive");
            $('#subMenu ul:visible').slideToggle('normal');
            checkElement.slideToggle('normal');
            return false;
        }

        if ($(this).siblings('ul').length == 0 && $(this).parent().parent().attr('id') == 'subMenu') {

            removeActiveClassFromAll();
            $(this).addClass("subMenuactive");
            $('#subMenu ul:visible').slideToggle('normal');

            return false;
        }
    });
}

function removeActiveClassFromAll() {
    $('#subMenu li a').each(function (index) {
        $(this).removeClass("subMenuactive");
    });
}


$(document).ready(function () {
    initMenu();
});

$('#subMenu').click(function (e) {
    e.stopPropagation();


})

$(document).click(function () {
    $('#subMenu').children('li').each(function () {
        if ($(this).children('ul').css('display') == 'block') {
            $(this).children('ul').slideToggle('normal')
            $(this).children('a').removeClass('subMenuactive')
        }
    })
});


/*Font family*/
WebFontConfig = {
    google: { families: ['Roboto+Slab:400,300:latin'] }
};
(function () {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
})();



/*Fancy*/
$(document).ready(function () {

    $('.fancybox').fancybox();

    $('.fancybox-thumbs').fancybox({

        closeBtn: true,
        arrows: true,

        helpers: {
            thumbs: {
                width: 60,
                height: 60
            }
        }
    });
});

/*Mobil Menu*/
$(function () {
    $('nav#menu').mmenu({
        extensions: ['effect-slide-menu', 'pageshadow'],
        searchfield: false,
        counters: false,
        navbar: {
            title: 'Karya Akademi'
        },
        navbars: [
          {
                position: 'top',
                content: [
                    'prev',
                    'title',
                    'close'
                ]
            } 
        ]
    });
});
