﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace AbbaraSite
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
            name: "Sayfa",
            url: "sayfa/{id}",
            defaults: new { controller = "Home", action = "Sayfa", id = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "PeopleDetail",
            url: "peoples/{id}/{namesurname}",
            defaults: new { controller = "Home", action = "Peoples", id = UrlParameter.Optional, namesurname = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "AnnouncementDetail",
            url: "announcements/{id}/{dataid}",
            defaults: new { controller = "Home", action = "Announcements", id = UrlParameter.Optional, dataid = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "PressDetail",
            url: "press/{id}/{dataid}",
            defaults: new { controller = "Home", action = "Press", id = UrlParameter.Optional, dataid = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "PressKitDetail",
            url: "presskit/{id}/{dataid}",
            defaults: new { controller = "Home", action = "PressKit", id = UrlParameter.Optional, dataid = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "Visuals",
            url: "visuals/{id}/{gallerycategory}",
            defaults: new { controller = "Home", action = "Visuals", id = UrlParameter.Optional, gallerycategoryid = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "ProgramDetail",
            url: "program/{place}/{category}/{id}/{programdetail}",
            defaults: new { controller = "Home", action = "Program", place = UrlParameter.Optional, category = UrlParameter.Optional, id = UrlParameter.Optional, programdetail = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "Calendar",
            url: "Calendar",
            defaults: new { controller = "Home", action = "Calendar" }
            );

            routes.MapRoute(
            name: "CalendarList",
            url: "CalendarList/{place}/{category}/{id}",
            defaults: new { controller = "Home", action = "CalendarList", place = UrlParameter.Optional, category = UrlParameter.Optional, id = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "Search",
            url: "Search/{text}",
            defaults: new { controller = "Home", action = "Search", text = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "ChangeLanguage",
            url: "ChangeLanguage/{shortName}",
            defaults: new { controller = "Home", action = "ChangeLanguage", shortName = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "Default",
            url: "{action}/{id}",
            defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
