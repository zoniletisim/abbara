﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
namespace AbbaraCMS
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        //protected void Application_BeginRequest()
        //{
        //    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("tr-TR");
        //    System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("tr-TR");
        //}

        protected void Application_Error(object sender, EventArgs e)
        {
            //Exception ex =new Exception();
            //ex= Server.GetLastError();
            //var innerException = String.Empty;
            //if(ex.InnerException == null)
            //    innerException = null;
            //else
            //innerException = ex.InnerException.ToString();

            //DefaultMainProgress.ErrorLog(ex.Message,innerException,Request.Url.ToString(),ex.StackTrace,DateTime.Now);
            //Response.Redirect("/Default/Index");
        }
        protected void Session_Start(object sender, EventArgs e)
        {
            Session.Add("Login", 1);
        }

    }
}
