﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Script.Serialization;
using System.Data;
using DAL;
using DAL.Repo;
using System.Data.SqlClient;
using System.Web.Caching;
using System.Web.UI;
using System.Text;
using System.Web.UI.WebControls;
using System.Collections;

namespace AbbaraCMS.Controllers
{
    public class AdminController : Controller
    {
        private DatabaseConnection db = new DatabaseConnection();

        //
        // GET: /Admin/

        public void CreateSession()
        {
            if (Session["AdminLanguage"] == null)
            {
                Session["AdminLanguage"] = 1;
            }
        }

        [ChildActionOnly]
        public ActionResult Header()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            return PartialView();
        }

        [ChildActionOnly]
        public ActionResult LeftMenu()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.PanelMenu = main.GetPanelMenu();
            return PartialView(main);
        }

        public ActionResult Index()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            return View(main);
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string email, string password)
        {
            var result = AdminMainProgress.AdminLogin(email, password, db);

            if (result != null)
            {
                Session.Add("LoginId", result.Id);
                Session.Add("Name", result.Name);
                Session.Add("Surname", result.Surname);
                Session.Add("Login", result.Role);
                Session.Add("AdminLanguage", 1);
                return Json(true);
            }

            return Json(false);
        }

        [HttpPost]
        public ActionResult LogOut()
        {
            Session["LoginId"] = null;
            Session["Name"] = null;
            Session["Surname"] = null;
            Session["Login"] = null;
            return Json(true);
        }

        [ChildActionOnly]
        public ActionResult ChangeLanguage()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.Languages = main.GetLanguages();
            return PartialView(main);
        }

        [HttpPost]
        public ActionResult AdminLanguage(string language)
        {
            Session["AdminLanguage"] = language;
            return Json(language);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //PanelUsers Start
        public ActionResult PanelUsers()
        {
            CreateSession();
            var result = AdminMainProgress.PanelUsersDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("PanelUsers");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.PanelUsers = main.GetPanelUsers();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PanelUsersSave(FormCollection fc)
        {
            AdminMainProgress.PanelUsersSave(fc["Email"], fc["Password"], fc["Name"], fc["Surname"], int.Parse(fc["Role"]), int.Parse(Session["LoginId"].ToString()), fc["Active"], Session["Login"], db);
            return RedirectToAction("PanelUsers");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PanelUsersUpdate(FormCollection fc)
        {
            AdminMainProgress.PanelUsersUpdate(fc["Id"], fc["Email"], fc["Name"], fc["Surname"], int.Parse(fc["Role"]), int.Parse(Session["LoginId"].ToString()), fc["Active"], Session["Login"], db);
            return RedirectToAction("PanelUsers");
        }

        [HttpPost]
        public ActionResult PanelUsersIsActive(string Id, string Active)
        {
            var result = AdminMainProgress.PanelUsersIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }
        //PanelUsers End

        //PanelMenu Start
        public ActionResult PanelMenu()
        {
            CreateSession();
            var result = AdminMainProgress.PanelMenuDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("PanelMenu");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.PanelMenu = main.GetPanelMenu();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PanelMenuSave(FormCollection fc)
        {
            AdminMainProgress.PanelMenuSave(int.Parse(fc["ParentId"]), fc["Name"], fc["Url"], fc["Redirect"], int.Parse(fc["Access"]), fc["Active"], Session["Login"], db);
            return RedirectToAction("PanelMenu");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PanelMenuUpdate(FormCollection fc)
        {
            AdminMainProgress.PanelMenuUpdate(fc["Id"], int.Parse(fc["ParentId"]), fc["Name"], fc["Url"], fc["Redirect"], int.Parse(fc["Access"]), fc["Active"], Session["Login"], db);
            return RedirectToAction("PanelMenu");
        }

        [HttpPost]
        public ActionResult PanelMenuIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.PanelMenuIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult PanelMenuSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.PanelMenuSort(ids, Session["Login"], db);
            return Json("success");
        }
        //PanelMenu End

        //Languages Start
        public ActionResult Languages()
        {
            CreateSession();
            var result = AdminMainProgress.LanguagesDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Languages");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.Languages = main.GetLanguages();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult LanguagesSave(FormCollection fc)
        {
            AdminMainProgress.LanguagesSave(fc["Name"], fc["ShortName"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Languages");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult LanguagesUpdate(FormCollection fc)
        {
            AdminMainProgress.LanguagesUpdate(fc["Id"], fc["Name"], fc["ShortName"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Languages");
        }

        [HttpPost]
        public ActionResult LanguagesIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.LanguagesIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult LanguagesSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.LanguagesSort(ids, Session["Login"], db);
            return Json("success");
        }
        //Languages End

        //SocialMedia Start
        public ActionResult SocialMedia()
        {
            CreateSession();
            var result = AdminMainProgress.SocialMediaDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("SocialMedia");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.SocialMedia = main.GetSocialMedia();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SocialMediaSave(FormCollection fc)
        {
            AdminMainProgress.SocialMediaSave(fc["Name"], fc["Link"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("SocialMedia");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SocialMediaUpdate(FormCollection fc)
        {
            AdminMainProgress.SocialMediaUpdate(fc["Id"], fc["Name"], fc["Link"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("SocialMedia");
        }

        [HttpPost]
        public ActionResult SocialMediaIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.SocialMediaIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult SocialMediaSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.SocialMediaSort(ids, Session["Login"], db);
            return Json("success");
        }
        //SocialMedia End

        //MultiPreferences Start
        public ActionResult MultiPreferences()
        {
            CreateSession();
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.MultiPreferences = main.GetMultiPreferences();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult MultiPreferencesUpdate(FormCollection fc)
        {
            AdminMainProgress.MultiPreferencesUpdate(fc["Id"], fc["Title"], fc["Description"], fc["Keywords"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("MultiPreferences");
        }
        //MultiPreferences End

        //PeopleCategories Start
        public ActionResult PeopleCategories()
        {
            CreateSession();
            var result = AdminMainProgress.PeopleCategoriesDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("PeopleCategories");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.PeopleCategories = main.GetPeopleCategories();
            main.Pages = main.GetPages();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PeopleCategoriesSave(FormCollection fc)
        {
            AdminMainProgress.PeopleCategoriesSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["PageFriendlyUrl"], fc["Title"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("PeopleCategories");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PeopleCategoriesUpdate(FormCollection fc)
        {
            AdminMainProgress.PeopleCategoriesUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["PageFriendlyUrl"], fc["Title"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("PeopleCategories");
        }

        [HttpPost]
        public ActionResult PeopleCategoriesIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.PeopleCategoriesIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult PeopleCategoriesSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.PeopleCategoriesSort(ids, int.Parse(Session["AdminLanguage"].ToString()), Session["Login"], db);
            return Json("success");
        }
        //PeopleCategories End

        //Peoples Start
        public ActionResult Peoples()
        {
            CreateSession();
            var result = AdminMainProgress.PeoplesDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Peoples");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.Peoples = main.GetPeoples();
            main.PeopleCategories = main.GetPeopleCategories();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PeoplesSave(FormCollection fc)
        {
            AdminMainProgress.PeoplesSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["Name"], fc["Surname"], fc["Title"], fc["Email"], fc["Image"], fc["ShortContent"], fc["Content"], fc["FriendlyUrl"], fc["Keywords"], fc["Description"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Peoples");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PeoplesUpdate(FormCollection fc)
        {
            AdminMainProgress.PeoplesUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["Name"], fc["Surname"], fc["Title"], fc["Email"], fc["Image"], fc["ShortContent"], fc["Content"], fc["FriendlyUrl"], fc["Keywords"], fc["Description"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Peoples");
        }

        [HttpPost]
        public ActionResult PeoplesIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.PeoplesIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult PeoplesSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.PeoplesSort(ids, int.Parse(Session["AdminLanguage"].ToString()), Session["Login"], db);
            return Json("success");
        }
        //Peoples End

        //GalleryParentCategories Start
        public ActionResult GalleryParentCategories()
        {
            CreateSession();
            var result = AdminMainProgress.GalleryParentCategoriesDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("GalleryParentCategories");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.GalleryParentCategories = main.GetGalleryParentCategories();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult GalleryParentCategoriesSave(FormCollection fc)
        {
            AdminMainProgress.GalleryParentCategoriesSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), fc["Title"], fc["FriendlyUrl"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("GalleryParentCategories");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult GalleryParentCategoriesUpdate(FormCollection fc)
        {
            AdminMainProgress.GalleryParentCategoriesUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), fc["Title"], fc["FriendlyUrl"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("GalleryParentCategories");
        }

        [HttpPost]
        public ActionResult GalleryParentCategoriesIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.GalleryParentCategoriesIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult GalleryParentCategoriesSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.GalleryParentCategoriesSort(ids, int.Parse(Session["AdminLanguage"].ToString()), Session["Login"], db);
            return Json("success");
        }
        //GalleryParentCategories End

        //GalleryCategories Start
        public ActionResult GalleryCategories()
        {
            CreateSession();
            var result = AdminMainProgress.GalleryCategoriesDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("GalleryCategories");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.GalleryCategories = main.GetGalleryCategories();
            main.Gallery = main.GetGallery();
            main.GalleryParentCategories = main.GetGalleryParentCategories();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult GalleryCategoriesSave(FormCollection fc)
        {
            AdminMainProgress.GalleryCategoriesSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["Title"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("GalleryCategories");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult GalleryCategoriesUpdate(FormCollection fc)
        {
            AdminMainProgress.GalleryCategoriesUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["Title"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("GalleryCategories");
        }

        [HttpPost]
        public ActionResult GalleryCategoriesIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.GalleryCategoriesIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult GalleryCategoriesSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.GalleryCategoriesSort(ids, Session["Login"], db);
            return Json("success");
        }
        //GalleryCategories End

        //Gallery Start
        public ActionResult Gallery()
        {
            CreateSession();

            if (Request.QueryString["delete"] == "true")
            {
                var result = AdminMainProgress.GalleryDelete(Session["Login"], Request.QueryString["action"], Request.QueryString["deleteid"], db);
            }

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.Gallery = main.GetGallery();
            main.GalleryCategories = main.GetGalleryCategories();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult GallerySave(FormCollection fc, HttpPostedFileBase[] files)
        {
            foreach (HttpPostedFileBase file in files)
            {
                try
                {
                    string filename = System.IO.Path.GetFileName(file.FileName);
                    file.SaveAs(Server.MapPath("~/Content/gallery/" + filename));
                    string filepathtosave = "/content/gallery/" + filename;
                    AdminMainProgress.GallerySave(int.Parse(fc["ParentId"].ToString()), filepathtosave, int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
                }
                catch (Exception Ex)
                {
                    ViewBag.Message = Ex.Message;
                }
            }

            return RedirectToAction("GalleryCategories");
        }

        [HttpPost]
        public ActionResult GallerySort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.GallerySort(ids, Session["Login"], db);
            return Json("success");
        }

        //[HttpPost]
        //[ValidateInput(false)]
        //public ActionResult GalleryDelete(int Id)
        //{
        //    var result = AdminMainProgress.GalleryDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
        //    return RedirectToAction(Request.Url.AbsolutePath);
        //}
        //Gallery End

        //Pages Start
        public ActionResult Pages()
        {
            CreateSession();
            var result = AdminMainProgress.PagesDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Pages");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.Pages = main.GetPages();
            main.GalleryParentCategories = main.GetGalleryParentCategories();
            main.GalleryCategories = main.GetGalleryCategories();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PagesSave(FormCollection fc)
        {
            AdminMainProgress.PagesSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["Title"], fc["ShortContent"], fc["Content"], fc["Banner"], fc["Thumbnail"], fc["Image"], fc["Galleries"], fc["Video"], fc["File"], fc["StartDate"], fc["StartTime"], fc["EndDate"], fc["Link"], int.Parse(fc["FormId"]), fc["ShowMenu"], fc["FriendlyUrl"], fc["Keywords"], fc["Description"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Pages");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PagesUpdate(FormCollection fc)
        {
            AdminMainProgress.PagesUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["Title"], fc["ShortContent"], fc["Content"], fc["Banner"], fc["Thumbnail"], fc["Image"], fc["Galleries"], fc["Video"], fc["File"], fc["StartDate"], fc["StartTime"], fc["EndDate"], fc["Link"], int.Parse(fc["FormId"]), fc["ShowMenu"], fc["FriendlyUrl"], fc["Keywords"], fc["Description"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Pages");
        }

        [HttpPost]
        public ActionResult PagesIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.PagesIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult PagesSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.PagesSort(ids, int.Parse(Session["AdminLanguage"].ToString()), Session["Login"], db);
            return Json("success");
        }
        //Pages End

        //SupporterCategories Start
        public ActionResult SupporterCategories()
        {
            CreateSession();
            var result = AdminMainProgress.SupporterCategoriesDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("SupporterCategories");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.SupporterCategories = main.GetSupporterCategories();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SupporterCategoriesSave(FormCollection fc)
        {
            AdminMainProgress.SupporterCategoriesSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), int.Parse(fc["TypeId"].ToString()), fc["Title"], fc["Content"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("SupporterCategories");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SupporterCategoriesUpdate(FormCollection fc)
        {
            AdminMainProgress.SupporterCategoriesUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), int.Parse(fc["TypeId"].ToString()), fc["Title"], fc["Content"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("SupporterCategories");
        }

        [HttpPost]
        public ActionResult SupporterCategoriesIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.SupporterCategoriesIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult SupporterCategoriesSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.SupporterCategoriesSort(ids, int.Parse(Session["AdminLanguage"].ToString()), Session["Login"], db);
            return Json("success");
        }
        //SupporterCategories End

        //Supporters Start
        public ActionResult Supporters()
        {
            CreateSession();
            var result = AdminMainProgress.SupportersDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Supporters");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.Supporters = main.GetSupporters();
            main.SupporterCategories = main.GetSupporterCategories();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SupportersSave(FormCollection fc)
        {
            AdminMainProgress.SupportersSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["Title"], fc["Image"], fc["Url"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Supporters");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SupportersUpdate(FormCollection fc)
        {
            AdminMainProgress.SupportersUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["Title"], fc["Image"], fc["Url"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Supporters");
        }

        [HttpPost]
        public ActionResult SupportersIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.SupportersIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult SupportersSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.SupportersSort(ids, int.Parse(Session["AdminLanguage"].ToString()), Session["Login"], db);
            return Json("success");
        }
        //Supporters End

        //EventCategories Start
        public ActionResult EventCategories()
        {
            CreateSession();
            var result = AdminMainProgress.EventCategoriesDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("EventCategories");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.EventCategories = main.GetEventCategories();
            main.Pages = main.GetPages();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EventCategoriesSave(FormCollection fc)
        {
            AdminMainProgress.EventCategoriesSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["PageFriendlyUrl"], fc["Title"], fc["ShortContent"], fc["FriendlyUrl"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("EventCategories");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EventCategoriesUpdate(FormCollection fc)
        {
            AdminMainProgress.EventCategoriesUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["PageFriendlyUrl"], fc["Title"], fc["ShortContent"], fc["FriendlyUrl"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("EventCategories");
        }

        [HttpPost]
        public ActionResult EventCategoriesIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.EventCategoriesIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult EventCategoriesSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.EventCategoriesSort(ids, int.Parse(Session["AdminLanguage"].ToString()), Session["Login"], db);
            return Json("success");
        }
        //EventCategories End

        //Events Start
        public ActionResult Events()
        {
            CreateSession();
            var result = AdminMainProgress.EventsDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Events");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.Events = main.GetEvents();
            main.EventCategories = main.GetEventCategories();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EventsSave(FormCollection fc)
        {
            AdminMainProgress.EventsSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), int.Parse(fc["ParentId"].ToString()), int.Parse(fc["PlaceId"].ToString()), fc["Title"], fc["ShortContent"], fc["Content"], fc["Thumbnail"], fc["Image"], fc["Leaders"], fc["Participants"], Convert.ToDateTime(fc["StartDate"]), TimeSpan.Parse(fc["StartTime"]), Convert.ToDateTime(fc["EndDate"]), int.Parse(fc["FormId"].ToString()), fc["FriendlyUrl"], fc["Keywords"], fc["Description"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Events");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EventsUpdate(FormCollection fc)
        {
            AdminMainProgress.EventsUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), int.Parse(fc["ParentId"].ToString()), int.Parse(fc["PlaceId"].ToString()), fc["Title"], fc["ShortContent"], fc["Content"], fc["Thumbnail"], fc["Image"], fc["Leaders"], fc["Participants"], Convert.ToDateTime(fc["StartDate"]), TimeSpan.Parse(fc["StartTime"]), Convert.ToDateTime(fc["EndDate"]), int.Parse(fc["FormId"].ToString()), fc["FriendlyUrl"], fc["Keywords"], fc["Description"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Events");
        }

        [HttpPost]
        public ActionResult EventsIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.EventsIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }
        //Events End

        //Press Start
        public ActionResult Press()
        {
            CreateSession();
            var result = AdminMainProgress.PressDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Press");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.GalleryParentCategories = main.GetGalleryParentCategories();
            main.GalleryCategories = main.GetGalleryCategories();
            main.Press = main.GetPress();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PressSave(FormCollection fc)
        {
            AdminMainProgress.PressSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["Title"], fc["ShortContent"], fc["Content"], fc["Thumbnail"], fc["Image"], fc["Galleries"], fc["Video"], fc["File"], fc["StartDate"], fc["StartTime"], fc["EndDate"], fc["Link"], fc["FriendlyUrl"], fc["Keywords"], fc["Description"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Press");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PressUpdate(FormCollection fc)
        {
            AdminMainProgress.PressUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["Title"], fc["ShortContent"], fc["Content"], fc["Thumbnail"], fc["Image"], fc["Galleries"], fc["Video"], fc["File"], fc["StartDate"], fc["StartTime"], fc["EndDate"], fc["Link"], fc["FriendlyUrl"], fc["Keywords"], fc["Description"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Press");
        }

        [HttpPost]
        public ActionResult PressIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.PressIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult PressSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.PressSort(ids, int.Parse(Session["AdminLanguage"].ToString()), Session["Login"], db);
            return Json("success");
        }
        //Press End

        //Translates Start
        public ActionResult Translates()
        {
            CreateSession();
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.Translates = main.GetTranslates();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult TranslatesSave(FormCollection fc)
        {
            AdminMainProgress.TranslatesSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), fc["Description"], fc["Text"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Translates");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult TranslatesUpdate(FormCollection fc)
        {
            AdminMainProgress.TranslatesUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), fc["Description"], fc["Text"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Translates");
        }
        //Translates End

        //ContactForm Start
        public ActionResult ContactForm()
        {
            CreateSession();
            var result = AdminMainProgress.ContactFormDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("ContactForm");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.ContactForm = main.GetContactForm();
            return View(main);
        }

        [HttpPost]
        public ActionResult ContactFormIsRead(string Id, string Read)
        {
            CreateSession();
            var result = AdminMainProgress.ContactFormIsRead(Id, Read, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }
        //ContactForm End

        //WorkshopForm Start
        public ActionResult WorkshopForm()
        {
            CreateSession();
            var result = AdminMainProgress.WorkshopFormDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("WorkshopForm");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.WorkshopForm = main.GetWorkshopForm();
            return View(main);
        }

        [HttpPost]
        public ActionResult WorkshopFormIsRead(string Id, string Read)
        {
            CreateSession();
            var result = AdminMainProgress.WorkshopFormIsRead(Id, Read, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }
        //WorkshopForm End

        //VolunteerForm Start
        public ActionResult VolunteerForm()
        {
            CreateSession();
            var result = AdminMainProgress.VolunteerFormDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("VolunteerForm");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.VolunteerForm = main.GetVolunteerForm();
            return View(main);
        }

        [HttpPost]
        public ActionResult VolunteerFormIsRead(string Id, string Read)
        {
            CreateSession();
            var result = AdminMainProgress.VolunteerFormIsRead(Id, Read, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }
        //VolunteerForm End

        //Newsletter Start
        public ActionResult Newsletter()
        {
            CreateSession();
            var result = AdminMainProgress.NewsletterDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Newsletter");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.Newsletter = main.GetNewsletter();
            return View(main);
        }

        [HttpPost]
        public ActionResult NewsletterIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.NewsletterIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }
        //Newsletter End

        //Other Start
        public ActionResult NewsletterToExcel()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var data = new System.Data.DataTable("E-Bülten Abonelikleri");
            data.Columns.Add("Email", typeof(string));

            foreach (var item in main.GetNewsletter())
            {
                data.Rows.Add(item.Email);
            }

            var grid = new GridView();
            grid.DataSource = data;
            grid.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=Newsletter " + DateTime.Now + ".xls");
            Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = Encoding.UTF8;
            Response.BinaryWrite(Encoding.UTF8.GetPreamble());
            Response.Charset = "";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            grid.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

            return View();
        }

        [HttpGet]
        public JsonResult GetSubTest(int ParentId)
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            ViewBag.lang = int.Parse(Session["AdminLanguage"].ToString());
            main.GalleryCategories = main.SiteGetGalleryCategories(ViewBag.lang);
            List<GalleryCategories> data = new List<GalleryCategories>();

            foreach (var item in main.GalleryCategories.Where(x => x.ParentId == ParentId).OrderBy(x => x.Title))
            {
                data.Add(item);
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //Other End
    }
}