﻿$(document).ready(function ()
{
    AdminAdd();
    PressOrNewAdd();
    PressOrNewDelete();

});
function PressOrNewDelete()
{
    $('#iceriksil').click(function () {
        var selectedvalue = $('#haberbasinicerik option:selected').val();
        $('#haberbasinicerik option:selected').remove();
        $.ajax({
            url: '/Admin/PressAndNewRemove',
            type: 'POST',
            dataType: 'json',
            data: { pageId: selectedvalue },
            success: function (result) {
                        alert(result)
            },
            error:function(err)
            {
                alert(err);
            }
        });
    });
}
function PressOrNewAdd()
{
    $('#PressOrNewSubmit').click(function () {

        var categoryurl=$('#kategoriId').val();
        var videourl=$('#video').val();
        var title=$('#baslik').val();
        var showorhide=$('#isShowHomepage').is(':checked');
        var content=$('#icerik').val();
        if (showorhide) {
            showorhide = "true";
        }
        else
            showorhide = "false";
        $.ajax({
            type:'POST',
            url: '/Admin/PressAndNew',
            dataType: 'json',
            data: {
                categoryurl: categoryurl, title: title, videourl:videourl , showorhide: showorhide, content: content
            },
           
            success:function (result) {
    
            },
           error:function (err) {
    
        }
   })
        
});
}
function AdminAdd()
{
    $('.adminsave').click(function () {

        var username = $('.username').val();
        var password = $('.password').val();


        $.ajax({
            type: 'POST',
            url: '/Admin/AdminSave',
            dataType: 'json',
            data: { username: username, password: password },
            success: function (result) {
                alert(result);
            },
            error: function () {

                alert("Bir hata meydana geldi daha sonra tekrar deneyin");
            }

        });
    });
}