﻿$(document).ready(function () {
    $('#ChangeLanguage').change(function () {
        $.ajax({
            url: '/Admin/AdminLanguage',
            type: 'POST',
            dataType: 'Json',
            data: { language: $(this).val() },
            success: function (data) {
            },
            error: function (err) {
            }
        });
        location.reload();
    });

    $('#login').click(function () {
        var email = $('#req1').val();
        var password = $('#req2').val();
        if (email != "" && password != "") {

            $.ajax({
                type: 'POST',
                url: '/Admin/Login',
                dataType: 'Json',
                data: { email: email, password: password },
                success: function (data) {
                    if (data == true)
                        window.location.href = "/Admin/Index";
                    else
                        alert("Kullanıcı adı veya şifre yanlış!");
                },
                error: function (err) {
                    alert(err);
                }
            });

        }
    });

    $('#logout').click(function () {
        $.ajax({
            type: 'POST',
            url: '/Admin/LogOut',
            dataType: 'Json',
            success: function (data) {
                if (data == true)
                    window.location = "/Admin/Login";
            },
            error: function (err) {
            }
        });
    });

    //PanelUsers End
    $('.panelusersIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/PanelUsersIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });
    //PanelUser End

    //PanelMenu End
    $('.panelmenuIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/PanelMenuIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#PanelMenuSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/PanelMenuSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //PanelMenu End

    //SocialMedia End
    $('.socialmediaIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/SocialMediaIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#SocialMediaSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/SocialMediaSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //SocialMedia End

    //Languages End
    $('.languagesIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/LanguagesIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#LanguagesSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/LanguagesSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //Languages End

    //PeopleCategories End
    $('.peoplecategoriesIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/PeopleCategoriesIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#PeopleCategoriesSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/PeopleCategoriesSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //PeopleCategories End

    //Peoples End
    $('.peoplesIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/PeoplesIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#PeoplesSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/PeoplesSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //Peoples End

    //Newsletter End
    $('.newsletterIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/NewsletterIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });
    //Newsletter End

    //GalleryParentCategories End
    $('.galleryparentcategoriesIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/GalleryParentCategoriesIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#GalleryParentCategoriesSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/GalleryParentCategoriesSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //GalleryParentCategories End

    //GalleryCategories End
    $('.GalleryCategoriesIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/GalleryCategoriesIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#GalleryCategoriesSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/GalleryCategoriesSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //GalleryCategories End

    //Gallery End
    $('#GallerySaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/GallerySort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //Gallery End

    //Pages End
    $('.pagesIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/PagesIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#PagesSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/PagesSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //Pages End

    //SupporterCategories End
    $('.supportercategoriesIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/SupporterCategoriesIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#SupporterCategoriesSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/SupporterCategoriesSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //SupporterCategories End

    //Supporters End
    $('.supportersIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/SupportersIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#SupportersSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/SupportersSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //Supporters End

    //EventCategories End
    $('.eventcategoriesIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/EventCategoriesIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#EventCategoriesSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/EventCategoriesSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //EventCategories End

    //Events End
    $('.eventsIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/EventsIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });
    //Events End

    //Press End
    $('.pressIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/PressIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#PressSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/PressSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //Press End

    //ContactForm End
    $('.contactformIsRead').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/ContactFormIsRead',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Read: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });
    //ContactForm End

    //WorkshopForm End
    $('.workshopformIsRead').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/WorkshopFormIsRead',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Read: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });
    //WorkshopForm End

    //VolunteerForm End
    $('.volunteerformIsRead').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/VolunteerFormIsRead',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Read: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });
    //VolunteerForm End
});