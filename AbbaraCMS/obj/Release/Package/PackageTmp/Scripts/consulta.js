﻿$(document).ready(function () {

    SomeSettings();
    PartFill();

    $('.ulke').change(function () {
       
        if ($(this).val() == 238)     // 238'ın anlamı Türkiye demek diğer ülkelerin İlleri olmadığı için getirmiyoruz.
        {

            $('.sehir').show();

            $.ajax({

                type: "POST",
                url: "/User/GetCity",
                dataType: "json",
                data: "countryId=" + 240,
                success: function (result) {
                    $.each(result, function (index, value) {
                        $('.sehir').append('<option value="' + value.cityId + '">' + value.cityName + '</option>');

                    });

                },
                error: function err() {
                    alert("Talep esnasinda bir sorun oluştu");
                }
            });
        }
        else {
            $('.sehir').hide();
        }
    });

 // Sehir Getir

    $('.gonder').eq(0).click(function () {

        var PeopleTitle = $('.unvan').eq(0).val();
        var PeopleFirstName = $('.ad').eq(0).val();
        var PeopleLastname = $('.soyad').eq(0).val();
        var PeopleCompany = $('.sirket').eq(0).val();
        var PeopleCompanyTitle = $('.isunvan').eq(0).val();
        var PeoplePhone = $('.telefon').eq(0).val();
        var PeopleEmail = $('.eposta').eq(0).val();
        var PeoplePart = $('.bolum').eq(0).val();
        var PeopleContent = $('.aciklama').eq(0).val();
        var PeopleCountry = $('.ulke').eq(0).val();
        var PeopleCity = $('.sehir').eq(0).val();
        var PeopleSubject = $('.konu').eq(0).val();
     
        $.ajax({
            type: 'POST',
            url: '/User/SendMail',
            dataType: 'Json',
            data: {
                email: PeopleEmail,
                firstname: PeopleFirstName,
                lastname: PeopleLastname,
                gender:PeopleTitle,
                company: PeopleCompany,
                peopletitle: PeopleCompanyTitle,
                phone: PeoplePhone,
                part: PeoplePart,
                content: PeopleContent,
                country: PeopleCountry,
                city: PeopleCity,
                subject: PeopleSubject
            },
            success: function (result) {
                alert(result);
            }
           
        })
    });   // Mail Yollama
    $('.gonder').eq(1).click(function () {
        var PeopleTitle = $('.unvan').eq(1).val();
        var PeopleFirstName = $('.ad').eq(1).val();
        var PeopleLastname = $('.soyad').eq(1).val();
        var PeopleCompany = $('.sirket').eq(1).val();
        var PeopleCompanyTitle = $('.isunvan').eq(1).val();
        var PeoplePhone = $('.telefon').eq(1).val();
        var PeopleEmail = $('.eposta').eq(1).val();
        var PeoplePart = $('.bolum').eq(1).val();
        var PeopleContent = $('.aciklama').eq(1).val();

        $.ajax({

            type: 'POST',
            url: '/User/WantOffOffer',
            dataType: 'json',
            data: {

                email: PeopleEmail,
                firstname: PeopleFirstName,
                lastname: PeopleLastname,
                gender: PeopleTitle,
                company: PeopleCompany,
                peopletitle: PeopleCompanyTitle,
                phone: PeoplePhone,
                part: PeoplePart,
                content: PeopleContent,
            },
            success: function () {

            }                 ,
            error:function (err) {
                alert(err);
            }

        })
    });
    $('.UyeOl').click(function ()
    {
        var PeopleFirstName = $('.ad').eq(0).val();
        var PeopleLastname = $('.soyad').eq(0).val();
        var PeopleCompany = $('.sirket').eq(0).val();
        var PeoplePhone = $('.telefon').eq(0).val();
        var PeopleEmail = $('.eposta').eq(0).val();
        var PeopleCountry = $('.ulke').eq(0).val();
        var PeopleCity = $('.sehir').eq(0).val();
        var PeopleCompanyTitle = $('.isunvan').eq(0).val();
        var adres = $('.adres').val();
        $.ajax({
            type: 'POST',
            url: '/User/Save',
            dataType: 'Json',
            data: {
                email: PeopleEmail,
                firstname: PeopleFirstName,
                lastname: PeopleLastname,
                company: PeopleCompany,
                phone: PeoplePhone,
                country: PeopleCountry,
                city: PeopleCity,
                mission: PeopleCompanyTitle,
                adress:adres,
            },
            success: function (result) {
                alert(result);
            }
    });
    });
});

function SomeSettings() {

    $('.ulke option').each(function (index, value) {
        $(this).val(index);
    });
    $('.sehir').hide();
    $('.bolum  option').remove();

    
}

function PartFill()
{
    $.ajax({
        type: 'POST',
        url: '/User/GetPart',
        dataType: 'Json',
        success: function(result) {
            $.each(result, function (index, value) {
                $('select.bolum').append('<option value="' + value.appPartId + '">' + value.appPartName + '</option>');

            });
        }
    })
}