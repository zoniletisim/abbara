﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AbbaraCMS.Startup))]
namespace AbbaraCMS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
